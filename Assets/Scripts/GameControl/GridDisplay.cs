﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GridDisplay : MonoBehaviour
{
    public GameObject playerM, playerD, playerW, enemyM, enemyD, enemyW;
    public float countdown;
    public GameObject CountDownText, InfoText, InfoCanvas;
    public bool countdownDone = false;
    public int phase = 3,toggle = 0;
    System.Random rnd = new System.Random();
     
    private void Start()
    {
        countdown = 5.9f;
        foreach (Character c in Server.getPlayers())
        {
            if (c.isAPlayer())
            {
                if (c.getType() == "Warrior")
                    instantiateCharacter(playerW,c);
                else if (c.getType() == "Mage")
                    instantiateCharacter(playerM,c);
                else if (c.getType() == "Dwarf")
                    instantiateCharacter(playerD,c);

                try
                {
                    Camera.main.GetComponent<CameraControl>().player = GameObject.Find(c.getName() + "." + c.getId()).transform;
                }
                catch (System.Exception)
                {

                    throw;
                }
            }
            else
            {
                if (c.getType() == "Warrior")
                    instantiateCharacter(enemyW,c);
                else if (c.getType() == "Mage")
                    instantiateCharacter(enemyM,c);
                else if (c.getType() == "Dwarf")
                    instantiateCharacter(enemyD,c);
                }
        }

    }

    private void Update()
    {

        switch (phase) {

            case 0://player inputing slot actions
                countdown -= Time.deltaTime;
                CountDownText.GetComponent<Text>().color = Color.red;
                CountDownText.GetComponent<Text>().text = "" + (int)countdown;
                break;

            case 1://processing ai slot actions
                GenerateAISlots();
                Server.setupSlotActions();
                
                phase = 2;
                CountDownText.GetComponent<Text>().text = "";
                break;

            case 2://displaying animations

                break;

            case 3://5sces temporary break
                countdown -= Time.deltaTime;
                CountDownText.GetComponent<Text>().color = Color.green;
                CountDownText.GetComponent<Text>().text = ""+(int)countdown;
                if (countdown <= 1f)
                    phase = 0;
                break;

            case 4://you win
                countdown -= Time.deltaTime;
                CountDownText.GetComponent<Text>().color = Color.green;
                CountDownText.GetComponent<Text>().text = "!!You Won!! congratulations!!";
                if (countdown <= 1f)
                    SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
                break;

            case 5://you lose
                countdown -= Time.deltaTime;
                CountDownText.GetComponent<Text>().color = Color.red;
                CountDownText.GetComponent<Text>().text = "!!You were Defeated!!";
                if (countdown <= 1f)
                    SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
                break;
        }

        UpdateInfoText(toggle);

    }

    private void UpdateInfoText(int toggle) {
        string information = "";

        switch (toggle)
        {
            case 0://display all user info
                foreach (Character c in Server.getLivePlayers())
                {
                    information += c.status();
                    information += "---------------------------------------\n";
                }
                InfoText.GetComponent<Text>().text = information;
                break;
            case 1://display player spell info
                foreach (Spell s in Server.getPlayers()[0].getSpells())
                {
                    information += s.spellInfo();
                    information += "---------------------------------------\n";
                }
                InfoText.GetComponent<Text>().text = information;
                break;
            case 2://hide info
                InfoCanvas.SetActive(false);
                break;
            case 3:
                InfoCanvas.SetActive(true);
                toggle = 0;
                break;
        }


    }

    private void UpdatePositions() {
        //TODO: remove gameobject if player died
        foreach (Character c in Server.getLivePlayers()) {
            Transform cGOT = GameObject.Find(c.getName() + "." + c.getId()).transform;
            cGOT.Translate(new Vector3(c.getPositionX() - cGOT.position.x,0,c.getPositionY() - cGOT.position.z),Space.World);
        }

    }

    public void ToggleInfo() {

        toggle++;
        switch (toggle) {
            case 0://display all user info
                InfoCanvas.SetActive(true);
                break;
            case 1://display player spell info
                InfoCanvas.SetActive(true);
                break;
            case 2://hide info
                InfoCanvas.SetActive(false);
                break;
            case 3:
                InfoCanvas.SetActive(true);
                toggle = 0;
                break;
        }
        

    }

    public void instantiateCharacter(GameObject prefab,Character c)
    {
        c.setGameObject(Instantiate(prefab, new Vector3(c.getPositionX(), prefab.transform.position.y, c.getPositionY()), prefab.transform.rotation) as GameObject);
        c.getGameObject().name = c.getName() + "." + c.getId();
    }

    private void GenerateAISlots() {

        //Get all ai players in game
        List<Character> AIPlayers = new List<Character>();
        foreach (Character c in Server.getLivePlayers()) {
            if (!c.isAPlayer()) {
                AIPlayers.Add(c);    
            }
        }

        //generate slotActions
        foreach (Character c in AIPlayers) {
            if (c.getType() == "Warrior") {
                Server.AddPlayerSlotActions(new WarriorAI(c,rnd).generateSlots());
            }
            if (c.getType() == "Mage") {
                Server.AddPlayerSlotActions(new MageAI(c,rnd).generateSlots());
            }
            if (c.getType() == "Dwarf") {
                Server.AddPlayerSlotActions(new DwarfAI(c,rnd).generateSlots());
            }

        }

        foreach (List<SlotAction> sActions in Server.allSlotActions) {
            foreach (SlotAction s in sActions)
            {
                print(s.toString());
            }
        }

        CountDownText.GetComponent<Text>().text = "Displaying Animations";

    }
}