﻿public class SlotAction{

    Character caster;
    Character recepient;
    char smarcChoice;
    float x = 0;
    float y = 0;
    string spellName = "";
    int slot;
    float speed,actualSpeed;
    bool isValid = true;

    public SlotAction() { }

    public SlotAction(bool isValid, int slot) {
        this.isValid = isValid;
        this.slot = slot;
    }

    public SlotAction(Character caster, Character recepient, char smarcChoice, float x, float y, string spellName, int slot, float speed)
    {
        this.caster = caster;
        this.recepient = recepient;
        this.smarcChoice = smarcChoice;
        if (smarcChoice == 'm')
        {
            this.x = x - caster.getPositionX();
            this.y = y - caster.getPositionY();
        }
        else {
            this.x = x;
            this.y = y;

        }

        this.spellName = spellName;
        this.slot = slot;
        this.actualSpeed = speed;
        fetchSpeed(speed);
    }

    public SlotAction(Spell spell)
    {
    this.caster = Server.getPlayers()[spell.getCasterID()];
    this.recepient = Server.getPlayers()[spell.getRecepientID()];
        this.smarcChoice = 's';
        this.x = 0;
        this.y = 0;
        this.spellName = spell.getName();
    }

    public string toString()
    {
        if (isValid){
            return string.Format("Caster: {0}, Recepient: {1}, SMARC: {2} X: {3}, " +
                        "Y: {4}, Spell: {5}, Speed: {6}, ActualSPD: {7}",
            (getCaster() != null ? getCaster().getName() : ""),
            (getRecepient() != null ? getRecepient().getName() : ""),
            getSmarcChoice(), getX(), getY(), getSpellName(), getSpeed(),getActualSpeed());
        }
        else{
            return "invalid slot action "+slot;
        }
    }

    public void fetchSpeed(float tempSpeed) {
        this.speed = (10 - tempSpeed) + (10 * slot);
        this.speed /= 2;
        switch (smarcChoice) {

            case 's':
                this.speed += .5f;
                break;
            case 'm':
                if (getCaster().getType() == "Warrior")
                    this.speed += .5f;
                if (getCaster().getType() == "Mage")
                    this.speed += 1f;
                if (getCaster().getType() == "Dwarf")
                    this.speed += 1f;
                break;
            case 'a':
                if (getCaster().getType() == "Warrior")
                    this.speed += .5f;
                if (getCaster().getType() == "Mage")
                    this.speed += 1f;
                if (getCaster().getType() == "Dwarf")
                    this.speed += 1.5f;
                break;
            case 'r':
                this.speed += 2f; 
                break;
            case 'c':
                this.speed += 2f;
                break;
        }
        
    }

    public Character getCaster()
    {
        return caster;
    }
    public void setCaster(Character caster)
    {
        this.caster = caster;
    }
    public Character getRecepient()
    {
        return recepient;
    }
    public void setRecepient(Character recepient)
    {
        this.recepient = recepient;
    }
    public char getSmarcChoice()
    {
        return smarcChoice;
    }
    public void setSmarcChoice(char smarcChoice)
    {
        this.smarcChoice = smarcChoice;
    }
    public float getX()
    {
        return x;
    }
    public void setX(float x)
    {
        this.x = x;
    }
    public float getY()
    {
        return y;
    }
    public void setY(float y)
    {
        this.y = y;
    }
    public string getSpellName()
    {
        return spellName;
    }
    public void setSpellName(string spellName)
    {
        this.spellName = spellName;
    }

    public int getSlot()
    {
        return slot;
    }

    public void setSlot(int slot)
    {
        this.slot = slot;
    }

    public float getSpeed()
    {
        return speed;
    }

    public void setSpeed(float speed)
    {
        this.speed = speed;
    }
    public float getActualSpeed()
    {
        return actualSpeed;
    }

    public void setActualSpeed(float speed)
    {
        this.actualSpeed = speed;
    }

    public bool IsValid()
        {
            return isValid;
        }
}
