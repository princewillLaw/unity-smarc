﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Client : MonoBehaviour
{
    //SlotAction Variables
    public Character Player;
    public Character Recipient;
    public char Smarc;
    public float X;
    public float Y;
    public int SpellIndex;

    bool hasSpell=false,hasCoor=false;

    //List of Spell names, player names, player characters and slot actions
    List<string> spellNames;
    List<SlotAction> SlotActions;

    //player UIs
    public Button[] SmarcButtons = new Button[5];
    public GameObject ControlPanel;
    public GameObject SpellListPanel;
    public Button[] SpellButtons = new Button[5];
    public Button Back;
    public Text info;

    //Server turn or player turn
    bool isListClear = false,startedSlot = false;

    //update variables
    private const int TOTAL_PLAYER_SLOTS = 3;

    //range gameobject
    private GameObject range;

    //functions
  
    private void settingSpellLists() {

        spellNames = new List<string>();
        for (int i=0;i< Player.getSpells().Count;i++)
        {
            if (Player.getSpells()[i].checkDisplayable()) {
                String name = Player.getSpells()[i].getName();
                spellNames.Add(name);
                SpellButtons[i].transform.GetChild(0).GetComponent<Text>().text = name;
            }
        }

        foreach (Button b in SpellButtons) {
            if (b.transform.GetChild(0).GetComponent<Text>().text == "...") {
                b.gameObject.SetActive(false);
            }
        }

    }
    private void InitialisingPlayer(){

        Player = Server.getPlayers()[0];
        SlotActions = new List<SlotAction>();
        settingSpellLists();

        
    }

    private void AddingUIListener(){

        SmarcButtons[0].onClick.AddListener(() => { SetSmarc("s"); });
        SmarcButtons[1].onClick.AddListener(() => { SetSmarc("m"); });
        SmarcButtons[2].onClick.AddListener(() => { SetSmarc("a"); });
        SmarcButtons[3].onClick.AddListener(() => { SetSmarc("r"); });
        SmarcButtons[4].onClick.AddListener(() => { SetSmarc("c"); });

        SpellButtons[0].onClick.AddListener(() => { SetSpellIndex(0); });
        SpellButtons[1].onClick.AddListener(() => { SetSpellIndex(1); });
        SpellButtons[2].onClick.AddListener(() => { SetSpellIndex(2); });
        SpellButtons[3].onClick.AddListener(() => { SetSpellIndex(3); });
        SpellButtons[4].onClick.AddListener(() => { SetSpellIndex(4); });


        Back.onClick.AddListener(() => { GoBack(); });

    }

    private void defaulting() {
        Recipient = null;
        Smarc = 'q';
        X = 0;
        Y = 0;
        SpellIndex =-1;
        ControlPanel.SetActive(true);
        SpellListPanel.SetActive(false);
        hasCoor = false;
        hasSpell = false;
        range.transform.localScale = new Vector3(1f, 1f, 1f);
        //setUIButtons();
    }

    public void GoBack() {
        //nothing selected this slot go to previous slot
        if (Smarc == 'q')
        {
            if (SlotActions.Count > 0 && SlotActions[SlotActions.Count - 1].IsValid())
            {
                SlotAction s = SlotActions[SlotActions.Count - 1];
                SlotActions.Remove(s);
                nextSlot(s.getActualSpeed());
            }

        }
        else {
            nextSlot(gameObject.GetComponent<GridDisplay>().countdown);
        }

        

    }

    private void Start()
	{
       InitialisingPlayer();
       AddingUIListener();
       range = Instantiate((GameObject)Resources.Load("Range", typeof(GameObject)));
       Vector3 playerPos = Player.getGameObject().transform.position;
       range.transform.position = new Vector3(playerPos.x,0.1f,playerPos.z);
    }

    private void nextSlot() {

        defaulting();
        gameObject.GetComponent<GridDisplay>().countdown = 10.99f;
        info.text = "Choose a Smarc for Slot "+(SlotActions.Count+1);

        if (SlotActions.Count > 2) {
            info.text = "...";
            gameObject.GetComponent<GridDisplay>().phase = 1;
            Server.AddPlayerSlotActions(SlotActions);
            isListClear = false;
        }
        disableUsedSmarcButton();
    }

    private void nextSlot(float remainingTime)
    {

        defaulting();
        gameObject.GetComponent<GridDisplay>().countdown = remainingTime;
        info.text = "Choose a Smarc for Slot " + (SlotActions.Count + 1);

        if (SlotActions.Count > 2)
        {
            info.text = "...";
            gameObject.GetComponent<GridDisplay>().phase = 1;
            Server.AddPlayerSlotActions(SlotActions);
            isListClear = false;
        }
        disableUsedSmarcButton();
    }

    private void Update()
	{

        if (gameObject.GetComponent<GridDisplay>().phase == 0)
        {
            clearList();
            if (!startedSlot)
            {
                nextSlot();
                startedSlot = true;
            }
            else
            {
                if (gameObject.GetComponent<GridDisplay>().countdown <= 1f)
                {
                    submitInvalidSlot();
                    startedSlot = false;
                }
            }
            if (Input.GetMouseButtonDown(0))
            {
                gettingClicks();
            }
            SetRange();
        }
        else {
            ControlPanel.SetActive(false);
        }
        
    }

    private void clearList() {
        if (!isListClear) {
            SlotActions.Clear();
            SlotActions = new List<SlotAction>();
            isListClear = true;
            startedSlot = false;
            enableSmarcButtons();
        }
    }

    public void SetSmarc(string smarc)
    {
        //Set the tracker variable
        Smarc = smarc[0];
        
        setUIButtons();
    }

    public void SetRecipient(String Name)
    {
        //Set the tacker variable
        foreach (Character c in Server.getLivePlayers())
        {
            if (c.getName() + "." + c.getId() == Name)
            {
                Recipient = c;
                setUIButtons();
                break;
            }

        }
    }

    public void SetSpellIndex(int index)
    {
        //set the tracker variable
        SpellIndex = index;
        hasSpell = true;
        setUIButtons();
    }

    public void SetXY(Vector3 point)
    {
        if (Smarc == 'm') {
            X = point.x;
            Y = point.z;
        }
        if (Smarc == 's')
        {
            X = (point.x + 0.5f);
            Y = (point.z + 0.5f);
        }
        hasCoor = true;
        setUIButtons();
    }

    private void enableSmarcButtons() {
        for (int i = 0; i < 5; i++) {

            SmarcButtons[i].enabled = true;
            SmarcButtons[i].interactable = true;
            SmarcButtons[i].transform.GetChild(0).GetComponent<Text>().color = Color.black;
        }
    }

    private void disableUsedSmarcButton() {
        enableSmarcButtons();
        foreach (SlotAction s in SlotActions) {
            
            if (s.getSmarcChoice() == 's') {
                SmarcButtons[0].enabled = false;
                SmarcButtons[0].interactable = false;
                SmarcButtons[0].transform.GetChild(0).GetComponent<Text>().color = Color.white;
            }
            if (s.getSmarcChoice() == 'm')
            {
                SmarcButtons[1].enabled = false;
                SmarcButtons[1].interactable = false;
                SmarcButtons[1].transform.GetChild(0).GetComponent<Text>().color = Color.white;
            }
            if (s.getSmarcChoice() == 'a')
            {
                SmarcButtons[2].enabled = false;
                SmarcButtons[2].interactable = false;
                SmarcButtons[2].transform.GetChild(0).GetComponent<Text>().color = Color.white;
            }
            if (s.getSmarcChoice() == 'r')
            {
                SmarcButtons[3].enabled = false;
                SmarcButtons[3].interactable = false;
                SmarcButtons[3].transform.GetChild(0).GetComponent<Text>().color = Color.white;
            }
            if (s.getSmarcChoice() == 'c')
            {
                SmarcButtons[4].enabled = false;
                SmarcButtons[4].interactable = false;
                SmarcButtons[4].transform.GetChild(0).GetComponent<Text>().color = Color.white;
            }

        }

    }

    private void gettingClicks() {

        if (Smarc == 's' && hasSpell)
        {
            if (Player.getSpells()[SpellIndex].checkRecepientNeeded() && Recipient == null)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    SetRecipient(hit.collider.name);
                }
            }
            else if (Player.getSpells()[SpellIndex].checkCoordinateNeeded() && !hasCoor)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    SetXY(hit.point);
                }
            }
        }
        if (Smarc == 'm' && !hasCoor)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                print(hit.point);
                SetXY(hit.point);
            }
        }
        if (Smarc == 'a' && Recipient == null)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                SetRecipient(hit.collider.name);
            }
        }

    }

    private void setUIButtons()
    {
        
        SpellListPanel.SetActive(false);

        if (Smarc.Equals('s'))
        {

            if (hasSpell)
            {

                //if the spell needs recipient and we dont have one
                if (Player.getSpells()[SpellIndex].checkRecepientNeeded() && Recipient == null)
                {
                    //hide smarc buttons
                    ControlPanel.SetActive(false);
                    //say need recipient
                    info.text = "Click " + Player.getSpells()[SpellIndex].getName() + "'s Recipient (within radius)";
                }
                else if (Player.getSpells()[SpellIndex].checkCoordinateNeeded() && !hasCoor)
                {
                    //hide smarc buttons
                    ControlPanel.SetActive(false);
                    //say need recipient
                    info.text = "Click  " + Player.getSpells()[SpellIndex].getName() + "'s coordinate (within radius)";
                } else if (isActionValid())
                {
                    addAction();
                }
                else
                {
                    //say invalid action
                    print("invalid action");
                    hasCoor = false;
                }
            }
            else {
                //display spell list if we dont have spell already
                SpellListPanel.SetActive(true);
            }



        }else if(Smarc.Equals('m')){

            if (!hasCoor)
            {
                //hide smarc buttons
                ControlPanel.SetActive(false);
                //say need recipient
                info.text = "Click where to move(within radius)";
            }
            else {
                if (isActionValid())
                {
                    addAction();
                }
                else
                {
                    //say invalid action
                    print("invalid action");
                    hasCoor = false;
                }
            }

        }else if(Smarc.Equals('a')){

            if (Recipient == null)
            {
                //hide smarc buttons
                ControlPanel.SetActive(false);
                //say need recipient
                info.text = "Click a Player to attack (within radius)";
            }
            else {
                addAction();
            }
        }else if (Smarc.Equals('r') || Smarc.Equals('c'))
        {
            addAction();
        }
    }

    private void SetRange() {
        Vector3 playerPos = Player.getGameObject().transform.position;
        range.transform.position = new Vector3(playerPos.x, 0.1f, playerPos.z);
        float radius;
        switch (Smarc) {
            case 's':
                range.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 0.45f);
                if (hasSpell)
                {
                    if (Player.getSpells()[SpellIndex].checkRecepientNeeded() && Recipient == null)
                    {
                        radius = Player.getSpells()[SpellIndex].getRange();
                        radius = radius < 0 ? 0 : radius;
                        range.transform.localScale = new Vector3(radius, radius, 1f);
                    }
                    else if (Player.getSpells()[SpellIndex].checkCoordinateNeeded() && !hasCoor)
                    {
                        radius = Player.getSpells()[SpellIndex].getRange();
                        radius = radius < 0 ? 0 : radius;
                        range.transform.localScale = new Vector3(radius, radius, 1f);
                    }
                }
                foreach (SlotAction sA in SlotActions)
                {
                    if (sA.getSmarcChoice() == 'm')
                    {
                        range.transform.position = new Vector3(Player.getGameObject().transform.position.x + sA.getX(), range.transform.position.y, Player.getGameObject().transform.position.z + sA.getY());
                        break;
                    }
                }
                break;
            case 'a':
                radius = Player.getAttackRange();
                radius = radius < 0 ? 0 : radius;
                range.GetComponent<SpriteRenderer>().color = new Color(1f, .8f, 0f, 0.45f);
                range.transform.localScale = new Vector3(radius, radius, 1f);
                foreach (SlotAction sA in SlotActions) {
                    if (sA.getSmarcChoice() == 'm') {
                        range.transform.position = new Vector3(Player.getGameObject().transform.position.x + sA.getX(), range.transform.position.y, Player.getGameObject().transform.position.z+ sA.getY());
                        break;
                    }
                }

                break;
            case 'm':
                radius = Player.getMovement();
                radius = radius < 0 ? 0 : radius;
                range.transform.localScale = new Vector3(radius, radius, 1f);
                range.GetComponent<SpriteRenderer>().color = new Color(0.6f, 0.6f, 0.6f, 0.45f);
                break;
            default:
                range.transform.position = new Vector3(Player.getGameObject().transform.position.x, range.transform.position.y, Player.getGameObject().transform.position.z);
                range.transform.localScale = new Vector3(0f, 0f, 1f);
                range.GetComponent<SpriteRenderer>().color = new Color(0.6f, 0.6f, 0.6f, 0.45f);
                break;
        }
    }

    private void submitInvalidSlot() {
        SlotActions.Add(new SlotAction(false, SlotActions.Count));
    }

    private bool isActionValid(){
        //TODO: check to see the current tracker variables are valid move
        bool isValid = true;

            //if its attack recipient should nt be null
            if(Smarc == 'a'){
                if(Recipient == null){
                    isValid = false;
                }
            }

            //if its move x and y shudnt be higher than movement
            if(Smarc == 'm'){
                Vector3 movePoint = new Vector3(X, Player.getGameObject().transform.position.y, Y);
                if (Server.OffsetPos(Player.getGameObject().transform.position, movePoint) > (Player.getMovement() / 2))
                {
                    isValid = false;
                }
            }

            //if its spell
            if(Smarc == 's'){
                //loop through all player spells
                foreach(Spell spell in Player.getSpells()){
                    isValid = false;
                    //check if spell is in player list
                    if(Player.getSpells()[SpellIndex] != null){
                        isValid = true;
                        //if spell need recipient check one exists
                        if(spell.checkRecepientNeeded() && Recipient == null)
                        {
                            isValid = false;
                        
                        }
                        //if spell need coordinate check x and y are above -1
                        if(spell.checkCoordinateNeeded() ){
                        if (X < 0 || Y < 0)
                        {
                            isValid = false;
                        }
                        }
                    break;
                    }
                }
            }

        if (Smarc != 's' && Smarc != 'm' && Smarc != 'a' && Smarc != 'r' && Smarc != 'c') {
            isValid = false;
        }
        return isValid;
   
    }

    public void addAction() {
            SlotAction newSlotAction = new SlotAction(Player, Recipient, Smarc, X, Y,(Smarc == 's' ? Player.getSpells()[SpellIndex].getName() : ""), SlotActions.Count, gameObject.GetComponent<GridDisplay>().countdown-1f);
            SlotActions.Add(newSlotAction);
            startedSlot = false;
    }
}