﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Server : MonoBehaviour{

    static List<Character> players = new List<Character>();
    public static List<List<SlotAction>> allSlotActions = new List<List<SlotAction>>();
    static List<Spell> activeSpells = new List<Spell>();
    const int slotsNumber = 3;
    static int readyCount = 0;
    public static List<SlotAction> finalSorted = new List<SlotAction>();
    public float timer = 1f;
    static bool executingSlots = false,hasActiveSpells = false;
    static GameObject rest,charge,attackFailed;

    private void Start()
    {
        rest = (GameObject)Resources.Load("Rest", typeof(GameObject));
        charge = (GameObject)Resources.Load("Charge", typeof(GameObject));
        attackFailed = (GameObject)Resources.Load("attackFailed", typeof(GameObject));
    }

    private void Update()
    {
        if (executingSlots) {
            timer += Time.deltaTime;
            gatherActiveSpells();
            runActiveSpells(timer);
            foreach (SlotAction sAction in finalSorted)
            {
                if (timer >= sAction.getSpeed())
                {
                    if (!sAction.getCaster().isDead()) {
                        switch (sAction.getSmarcChoice())
                        {
                            case 's':
                                executeSpell(sAction);
                                break;
                            case 'm':
                                executeMove(sAction);
                                break;
                            case 'a':
                                executeAttack(sAction);
                                break;
                            case 'r':
                                executeRest(sAction);
                                break;
                            case 'c':
                                executeCharge(sAction);
                                break;
                        }
                    }
                    finalSorted.Remove(sAction);
                    break;
                }
            }
        }

        if (timer>16f && executingSlots) {
            hasActiveSpells = false;
            gameObject.GetComponent<GridDisplay>().countdown = 5.9f;
            gameObject.GetComponent<GridDisplay>().phase = 3;
            foreach (Character c in Server.getPlayers())
            {
                print(c.tostring());
            }
            executingSlots = false;
            fixOverlappedPlayers();
            //broadcast a player was knocked out severe all connection with server
            //TODO: Allow player t still watch the match but not participate anymore
            foreach (Character c in Server.getPlayers())
            {
                if (c.isDead())
                {
                    if (c.isAPlayer())
                    {
                        //you lost
                        gameObject.GetComponent<GridDisplay>().phase = 5;
                        break;
                    }
                    else {
                        Destroy(c.getGameObject());

                    }

                }
            }
            if(Server.getLivePlayers().Count == 1 && Server.getLivePlayers()[0].isAPlayer())
            {
                gameObject.GetComponent<GridDisplay>().phase = 4;
            }
            allSlotActions.Clear();
            timer = 1f;
        }
    }
    public static void setupSlotActions()
    {
        finalSorted = new List<SlotAction>();
        foreach (List<SlotAction> playerSlots in allSlotActions)
        {
            foreach (SlotAction sAction in sortSlotActions(playerSlots))
            {
                if (isSlotActionPermitted(sAction))
                {
                    finalSorted.Add(sAction);
                }
            }
        }
        executingSlots = true;
        
    }


    public static void executeAttack(SlotAction action)
    {
        if (((getPlayers()[action.getCaster().getId()].getAttackRange() / 2)+0.5f) >=
            OffsetPos(getPlayers()[action.getCaster().getId()],
                      getPlayers()[action.getRecepient().getId()]))
        {
            //if attack should suceed
            getPlayers()[action.getRecepient().getId()].addHp(-
                getPlayers()[action.getCaster().getId()].getAttackDamage());
            try
            {
                action.getCaster().getGameObject().GetComponent<RotationScript>().target = action.getRecepient().getGameObject();
                action.getCaster().getGameObject().GetComponent<Animator>().SetTrigger("attack");
            }
            catch (Exception e) { print(e.Message); }
        }
        else {
            //if attack should fail
            Instantiate(attackFailed, action.getCaster().getGameObject().transform);
        }
    }

    public static void executeCharge(SlotAction action)
    {
        getPlayers()[action.getCaster().getId()].charge();
        Instantiate(charge, action.getCaster().getGameObject().transform);
    }

    public static void executeMove(SlotAction action)
    {
        getPlayers()[action.getCaster().getId()].moveTo(action.getX(), action.getY());
    }

    public static void executeRest(SlotAction action)
    {
        getPlayers()[action.getCaster().getId()].rest();
        Instantiate(rest, action.getCaster().getGameObject().transform);
    }

    public static void executeSpell(SlotAction action)
    {
        foreach (Spell spell in getPlayers()[action.getCaster().getId()].getSpells())
        {
            if (spell.getName().Equals(action.getSpellName()))
            {
                spell.cast( action.getCaster().getId(), 
                            (action.getRecepient() != null) ? action.getRecepient().getId() : action.getCaster().getId(), 
                            action.getX(),
                            action.getY(),
                            action.getSpeed());
                break;
            }
        }
    }

    //Helper functions

    public static float OffsetPos(Character a, Character b)
    {
        return Vector3.Distance(a.getGameObject().transform.position , b.getGameObject().transform.position);
    }

    public static float OffsetPos(GameObject a, GameObject b)
    {
        return Vector3.Distance(a.transform.position, b.transform.position);
    }

    public static float OffsetPos(Vector3 a, Vector3 b)
    {
        return Vector3.Distance(a, b);
    }

    public static void runActiveSpells(float time)
    { 
        List<Spell> ranSpells = new List<Spell>();
        foreach (Spell spell in activeSpells)
        {
            if (spell.getCastTime() <= time && spell.checkActive())
            {    
                if (!Server.getPlayers()[spell.getCasterID()].isDead())
                {
                    spell.run();
                }
                ranSpells.Add(spell);
            }
        }
        foreach (Spell s in ranSpells) {
            activeSpells.Remove(s);
        }
            
    }

    public static void gatherActiveSpells()
    {
        if (!hasActiveSpells)
        {
            foreach (Character c in Server.getPlayers())
            {
                foreach (Spell spell in c.getSpells())
                {
                    if (spell.checkActive())
                    {
                        activeSpells.Add(spell);
                    }
                }
            }
            hasActiveSpells = true;
        }

    }

    public static List<SlotAction> sortSlotActions(List<SlotAction> unSortedSlots)
    {
        List<SlotAction> sortedSlots = new List<SlotAction>();

        foreach (SlotAction sAction in unSortedSlots)
        {

            if (sortedSlots.Count < 1)
            {
                sortedSlots.Add(sAction);
            }
            else
            {
                //loop through current fake sorted slots
                for (int i = 0; i < sortedSlots.Count; i++)
                {
                    //slot action is faster than current index
                    if (sAction.getSpeed() < sortedSlots[i].getSpeed())
                    {
                        sortedSlots.Insert(i, sAction);
                        break;
                    }
                    
                    //if end of sorted list place and the end
                    if (i + 1 == sortedSlots.Count)
                    {
                        sortedSlots.Add(sAction);
                        break;
                    }
                }
            }
        }

        return sortedSlots;
    }

    public static bool isSlotActionPermitted(SlotAction sAction)
    {
        if (sAction.IsValid())
        {
            foreach (Character p in getLivePlayers())
            {
                if (p.getId() == sAction.getCaster().getId())
                {
                    return true;
                }
            }
        }

        return false;
    }

    public static void fixOverlappedPlayers()
    {
        foreach (Character player in getPlayers())
        {
            foreach (Character playerSecond in getPlayers())
            {
                //if not ourself
                if (player != playerSecond)
                {
                    //they overlap
                    if (player.getPositionX() == playerSecond.getPositionX() &&
                            player.getPositionY() == playerSecond.getPositionY())
                    {
                        if (playerSecond.getPositionX() < 23)
                        {
                            playerSecond.addPositionX(1);
                        }
                        else if (playerSecond.getPositionX() > 1)
                        {
                            playerSecond.addPositionX(-1);
                        }
                        else if (playerSecond.getPositionY() > 1)
                        {
                            playerSecond.addPositionY(-1);
                        }
                        else if (playerSecond.getPositionY() > 1)
                        {
                            playerSecond.addPositionY(-1);
                        }
                    }
                }
            }
        }
    }

    public static bool isNotGameOver()
    {
        //get first living player team
        int teamLeft = Server.getLivePlayers()[0].getTeamNumber();
        //return true if 2 living players have different teams
        foreach (Character player in getLivePlayers())
        {
            if (player.getTeamNumber() != teamLeft)
            {
                return true;
            }
        }
        //return false if all living players are same team
        return false;
    }

    //Getters and Setters

    public static void setPlayers(List<Character> newPlayers){
        players = newPlayers;
    }

    public static List<Character> getPlayers()
    {
        return players;
    }

    public static List<Character> getLivePlayers()
    {
        List<Character> livePlayers = new List<Character>();

        foreach(Character c in players){
            if(!c.isDead()){
                livePlayers.Add(c);
            }
        }

        return livePlayers;
    }

    public static void AddPlayerSlotActions(List<SlotAction> sActions){
        allSlotActions.Add(sActions);

        readyCount++;
    }

    public static bool allReady(){
        return readyCount == allSlotActions.Count+1;
    }


}
