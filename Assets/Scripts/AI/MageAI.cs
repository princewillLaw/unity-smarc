﻿using System.Collections.Generic;
using UnityEngine;


public class MageAI
{

    Character mage;
    System.Random rnd;
    public MageAI(Character mage, System.Random rnd)
    {
        this.mage = mage;
        this.rnd = rnd;
    }

    public List<SlotAction> generateSlots()
    {
        List<SlotAction> sActions = new List<SlotAction>();

        //setup recepient

        List<Character> EnemyPlayers = new List<Character>();
        List<Character> AllyPlayers = new List<Character>();
        foreach (Character p in Server.getLivePlayers())
        {
            if (mage.getTeamNumber() == p.getTeamNumber())
            {
                AllyPlayers.Add(p);
            }
            else
            {
                EnemyPlayers.Add(p);
            }
        }

        Character allyRecepient = AllyPlayers[rnd.Next(AllyPlayers.Count)];
        Character enemyRecepient = EnemyPlayers[rnd.Next(EnemyPlayers.Count)];

        //setup smarc choice index with smarc rules
        List<int> choiceIndexes = new List<int>();
        for (int i = 0; i < 3; i++)
        {
            int choiceIndex = rnd.Next(5);
            if (choiceIndexes.Contains(choiceIndex))
            {
                i--;
            }
            else
            {
                choiceIndexes.Add(choiceIndex);
            }
        }

        //run through choice indexes and get random slotactions
        for (int i = 0; i < choiceIndexes.Count; i++)
        {
            switch (choiceIndexes[i])
            {
                //cast a spell	
                case 0:
                    int mind = rnd.Next(3);
                    switch (mind)
                    {
                        case 0:
                            sActions.Add(new SlotAction(mage, enemyRecepient, 's', 0, 0, "Elemental Blast", i, rnd.Next(5, 10)));
                            break;
                        case 1:
                            sActions.Add(new SlotAction(mage, allyRecepient, 's', 0, 0, "Heal", i, rnd.Next(5, 10)));
                            break;
                        case 2:
                            sActions.Add(new SlotAction(mage, allyRecepient, 's', 0, 0, "Teleport", i, rnd.Next(5, 10)));
                            break;
                    }
                    break;
                //mount an attack	
                case 1:
                    sActions.Add(new SlotAction(mage, enemyRecepient, 'a', 0, 0, "", i, rnd.Next(5, 10)));
                    break;
                //move somewhere
                case 2:
                    float xMovement = (mage.getMovement() > 0 ? rnd.Next((int)mage.getMovement()/2) : 0);
                    float yMovement = (mage.getMovement()/2) - xMovement;
                    xMovement += mage.getPositionX();
                    yMovement += mage.getPositionY();
                    Debug.Log(xMovement + " " + yMovement);
                    sActions.Add(new SlotAction(mage, null, 'm', xMovement, yMovement, "", i, rnd.Next(5, 10)));
                    break;
                //rest	
                case 3:
                    sActions.Add(new SlotAction(mage, null, 'r', 0, 0, "", i, rnd.Next(5, 10)));
                    break;
                //charge	
                case 4:
                    sActions.Add(new SlotAction(mage, null, 'c', 0, 0, "", i, rnd.Next(5, 10)));
                    break;
                //rest	
                default:
                    sActions.Add(new SlotAction(mage, null, 'r', 0, 0, "", i, rnd.Next(5, 10)));
                    break;
            }
        }

        return sActions;
    }
}