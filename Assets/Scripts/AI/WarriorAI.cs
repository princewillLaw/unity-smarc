﻿using System.Collections.Generic;
using UnityEngine;


public class WarriorAI
{

    Character warrior;
    System.Random rnd;
    public WarriorAI(Character warrior, System.Random rnd)
    {
        this.warrior = warrior;
        this.rnd = rnd;
    }

    public List<SlotAction> generateSlots()
    {
        List<SlotAction> sActions = new List<SlotAction>();

        //setup recepient

        List<Character> EnemyPlayers = new List<Character>();
        List<Character> AllyPlayers = new List<Character>();
        foreach (Character p in Server.getLivePlayers())
        {
            if (warrior.getTeamNumber() == p.getTeamNumber())
            {
                AllyPlayers.Add(p);
            }
            else
            {
                EnemyPlayers.Add(p);
            }
        }

        Character allyRecepient = AllyPlayers[rnd.Next(AllyPlayers.Count)];
        Character enemyRecepient = EnemyPlayers[rnd.Next(EnemyPlayers.Count)];

        //setup smarc choice index with smarc rules
        List<int> choiceIndexes = new List<int>();
        for (int i = 0; i < 3; i++)
        {
            int choiceIndex = rnd.Next(5);
            if (choiceIndexes.Contains(choiceIndex))
            {
                i--;
            }
            else
            {
                choiceIndexes.Add(choiceIndex);
            }
        }

        //run through choice indexes and get random slotactions
        for (int i = 0; i < choiceIndexes.Count; i++)
        {
            switch (choiceIndexes[i])
            {
                //cast a spell	
                case 0:
                    int mind = rnd.Next(4);
                    switch (mind)
                    {
                        case 0:
                            sActions.Add(new SlotAction(warrior, null, 's', 0, 0, "Guardian", i, rnd.Next(5, 10)));
                            break;
                        case 1:
                            sActions.Add(new SlotAction(warrior, null, 's', 0, 0, "Long Sword", i, rnd.Next(5, 10)));
                            break;
                        case 2:
                            sActions.Add(new SlotAction(warrior, enemyRecepient, 's', 0, 0, "Spearce", i, rnd.Next(5, 10)));
                            break;
                        case 3:
                            sActions.Add(new SlotAction(warrior, null, 's', 0, 0, "Berserk", i, rnd.Next(5, 10)));
                            break;
                    }
                    break;
                //mount an attack	
                case 1:
                    sActions.Add(new SlotAction(warrior, enemyRecepient, 'a', 0, 0, "", i, rnd.Next(5, 10)));
                    break;
                //move somewhere
                case 2:
                    float xMovement = (warrior.getMovement() > 0 ? rnd.Next((int)warrior.getMovement()/2) : 0);
                    float yMovement = (warrior.getMovement()/2) - xMovement;
                    xMovement += warrior.getPositionX();
                    yMovement += warrior.getPositionY();
                    Debug.Log(xMovement + " " + yMovement);
                    sActions.Add(new SlotAction(warrior, null, 'm', xMovement, yMovement, "", i, rnd.Next(5, 10)));
                    break;
                //rest	
                case 3:
                    sActions.Add(new SlotAction(warrior, null, 'r', 0, 0, "", i, rnd.Next(5, 10)));
                    break;
                //charge	
                case 4:
                    sActions.Add(new SlotAction(warrior, null, 'c', 0, 0, "", i, rnd.Next(5, 10)));
                    break;
                //rest	
                default:
                    sActions.Add(new SlotAction(warrior, null, 'r', 0, 0, "", i, rnd.Next(5, 10)));
                    break;
            }
        }

        return sActions;
    }
}