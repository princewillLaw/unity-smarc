﻿using UnityEngine;

public class Upgrade : Spell {

    bool hasRan = false;
    bool isDeactivated = true;

    public Upgrade() : base(0, -35, 2, 0, -1, 5, 0, 0,
                10, 0, false, true, -0.25, 0, 0,
                            "Upgrade", true, 0, 0){
    }

    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        base.setup(castSlot);

        if (base.checkActive())
        {
            base.setActive(false);
            deactivate();
        }
        else
        {
            base.setInvolvedIDs(casterID, recepientID);
            base.setActive(true);
            hasRan = false;
            run();
        }

    }

    public override void run()
    {
        //check if magic is enough and spell is active
        if (base.isMagicEnough(Server.getPlayers()[getCasterID()].getMagic()) && (base.getRange()/2) >=
            Server.OffsetPos(Server.getPlayers()[base.getCasterID()],
                             Server.getPlayers()[base.getRecepientID()]))
        {
            isDeactivated = false;
            //reduce damage variation and movement only once
            if (!hasRan)
            {
                Server.getPlayers()[base.getRecepientID()].addDamageVariation(base.getDamageVariation());
                Server.getPlayers()[base.getRecepientID()].addAttackRange(base.getAttackRange());
                Server.getPlayers()[base.getRecepientID()].addRest(base.getRest());
                if(getCasterID() != getRecepientID())
                    Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<RotationScript>().target
                   = Server.getPlayers()[base.getRecepientID()].getGameObject();

                Server.getPlayers()[base.getCasterID()].getGameObject().AddComponent<Upgradeing>();
                Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<Upgradeing>().target
                    = Server.getPlayers()[base.getRecepientID()].getGameObject();
            }
            //decrease caster magic every turn its active
            Server.getPlayers()[base.getCasterID()].addMagic(base.getMagic());

            //check if its ran at all this turn
            hasRan = true;
        }
        else
        { //out of mana
            GameObject.Instantiate(getSpellFailed(), Server.getPlayers()[base.getCasterID()].getGameObject().transform);
            deactivate();
        }
    }

    public void deactivate()
    {
        if (!isDeactivated)
        {
            Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<Upgradeing>().isOn = false;
            Server.getPlayers()[base.getRecepientID()].addDamageVariation(-base.getDamageVariation());
            Server.getPlayers()[base.getRecepientID()].addAttackRange(-base.getAttackRange());
            Server.getPlayers()[base.getRecepientID()].addRest(-base.getRest());
            hasRan = false;
            isDeactivated = true;
        }
    }
    public override string spellInfo()
    {
        string information = getName() + "\n\n";
        information += "Info: Tinker about\n with your tools\nimproving self or allies.\n";
        information += "Magic drain: " + getMagic() + "per turn \n";
        information += "Damage variation: " + getDamageVariation() + "\n";
        information += "attack Range: " + getAttackDamage() + "\n";
        information += "Rest increase: " + getRest() + "\n";
        return information;
    }

}
