﻿using UnityEngine;

public class Teleport : Spell {


    bool hasRan = false;
    public Teleport() : base(0, -150, 0, 0, 0, 0, 0, 0,
                15, 0, false, true, 0.0, 0, 0,
                "Teleport", true, 0, 0){
        base.setCoordinateNeeded(true);
    }

    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        if (!base.checkActive())
        {
            base.setDuration(2);
            base.setX(x);
            base.setY(y);
            base.setup(castSlot);
            base.setInvolvedIDs(casterID, recepientID);
            base.setActive(true);
            run();
        }
        else
        {
            deactivate();
        }
    }

    public override void run()
    {
        //check if it hasn't run the first time
        if (!hasRan)
        {
            //check if magic is enough and recepient is in range of caster
            if (!base.isMagicEnough(Server.getPlayers()[base.getCasterID()].getMagic()) ||
                ((base.getRange()/2)+0.5f) < Server.OffsetPos(Server.getPlayers()[base.getCasterID()],
                                                   Server.getPlayers()[base.getRecepientID()]))
            {
                GameObject.Instantiate(getSpellFailed(), Server.getPlayers()[base.getCasterID()].getGameObject().transform);
                deactivate();
            }
            else
            {
                //take the magic needed
                Server.getPlayers()[base.getCasterID()].addMagic(base.getMagic());
                if (getCasterID() != getRecepientID())
                    Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<RotationScript>().target
                    = Server.getPlayers()[base.getRecepientID()].getGameObject();
                Server.getPlayers()[base.getCasterID()].getGameObject().AddComponent<Teleporting>();
                Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<Teleporting>().target
                    = Server.getPlayers()[base.getRecepientID()].getGameObject();
                hasRan = true;
            }
        }
        if (base.checkActive())
        {
            //take a duration away
            base.addDuration(-1);
            //if duration finishes
            if (base.getDuration() < 0)
            {
                //create a temp player to hold x and y teleport coordinates
                Vector3 recepientPos = Server.getPlayers()[base.getRecepientID()].getGameObject().transform.position;
                Vector3 tLocation = new Vector3(base.getX(),
                                                recepientPos.y, 
                                                base.getY());
                //check if caster is not too far from coordinates
                if (((base.getRange() / 2) + 0.5f) >= Server.OffsetPos(recepientPos,
                        tLocation))
                {
                    Server.getPlayers()[base.getRecepientID()].setPositionX(base.getX());
                    Server.getPlayers()[base.getRecepientID()].setPositionY(base.getY());
                    Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<Teleporting>().isOn = false;
                }
                deactivate();
            }
        }
    }

    public void deactivate()
    {
        //remove component and gameobject
        try
        {
            Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<Teleporting>().deactivate = true;
        }
        catch (System.Exception)
        {
            throw;
        }
        base.setActive(false);
        hasRan = false;
        base.setDuration(0);
        base.setX(0);
        base.setY(0);
    }

    public override string spellInfo()
    {
        string information = getName() + "\n\n";
        information += "Info: Change the battle position\nmove a piece\nSend your foes away\nbring your allies closer.\n";
        information += "Magic: " + getMagic() + "\n";
        information += "Delay: " + 2 + " turns\n";
        information += "Radius: " + getRange()/2 + "\n";
        return information;
    }

}
