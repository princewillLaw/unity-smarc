﻿using System;
using UnityEngine;


public abstract class Spell {
    
        private int Hp;
        private int Magic;
        private int AttackRange;
        private int AttackDamage;
        private float Movement;
        private int Rest;
        private int Charge;

        private int Duration;
        private float Range;
        private float Radius;
        private bool isActive = false;
        private bool isDisplayable = true;

        private double DamageVariation;

        private int recepient;
        private int caster;

        private string name;

        private bool isRecepientNeeded = false;
        private bool isCoordinateNeeded = false;
        private bool forEnemy = false;
        private float castSlot;

        private float x;
        private float y;

        public static System.Random rnd = new System.Random();

        public Spell(int hp, int magic, int attackRange, int attackDamage, float movement, int rest, int charge, int duration,
                float range, float radius, bool isActive, bool isDisplayable, double damageVariation, int recepient, int caster,
                string name, bool needsRecepient, float x, float y)
        {
            Hp = hp;
            Magic = magic;
            AttackRange = attackRange*2;
            AttackDamage = attackDamage;
            Movement = movement*2;
            Rest = rest;
            Charge = charge;
            Duration = duration;
            Range = range*2;
            Radius = radius*2;
            this.isActive = isActive;
            this.isDisplayable = isDisplayable;
            DamageVariation = damageVariation;
            this.recepient = recepient;
            this.caster = caster;
            this.name = name;
            this.isRecepientNeeded = needsRecepient;
            this.x = x;
            this.y = y;
        }

        public GameObject getSpellFailed() {
            return (GameObject)Resources.Load("spellFailed", typeof(GameObject));
        }

        public int getCasterID()
        {
            return caster;
        }

        public void setCasterID(int caster)
        {
            this.caster = caster;
        }

        public string getName()
        {
            return name;
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public int getHp()
        {
            return Hp;
        }

        public void setHp(int hp)
        {
            Hp = hp;
        }

        public int getMagic()
        {
            return Magic;
        }

        public void setMagic(int magic)
        {
            Magic = magic;
        }

        public int getAttackRange()
        {
            return AttackRange;
        }

        public void setAttackRange(int attackRange)
        {
            AttackRange = attackRange;
        }

        public int getAttackDamage()
        {
            return AttackDamage;
        }

        public void setAttackDamage(int attackDamage)
        {
            AttackDamage = attackDamage;
        }

        public float getMovement()
        {
            return Movement;
        }

        public void setMovement(float movement)
        {
            Movement = movement;
        }

        public int getRest()
        {
            return Rest;
        }

        public void setRest(int rest)
        {
            Rest = rest;
        }

        public int getCharge()
        {
            return Charge;
        }

        public void setCharge(int charge)
        {
            Charge = charge;
        }

        public int getDuration()
        {
            return Duration;
        }

        public void addDuration(int duration)
        {
            Duration += duration;
        }

        public void setDuration(int duration)
        {
            Duration = duration;
        }

        public float getRange()
        {
            return Range;
        }

        public void setRange(float range)
        {
            Range = range;
        }

        public float getRadius()
        {
            return Radius;
        }

        public void setRadius(float radius)
        {
            Radius = radius;
        }

        public double getDamageVariation()
        {
            return DamageVariation;
        }

        public void setDamageVariation(double damageVariation)
        {
            DamageVariation = damageVariation;
        }

        public bool checkActive()
        {
            return isActive;
        }

        public void setActive(bool isActive)
        {
            this.isActive = isActive;
        }

        public bool checkDisplayable()
        {
            return isDisplayable;
        }


        public void setDisplayable(bool isDisplayable)
        {
            this.isDisplayable = isDisplayable;
        }


        public int getRecepientID()
        {
            return recepient;
        }

        public void setRecepientID(int recepient)
        {
            this.recepient = recepient;
        }

        public void setInvolvedIDs(int casterID, int recepientID)
        {
            setCasterID(casterID);
            setRecepientID(recepientID);
        }

        public bool isMagicEnough(int playerMagic)
        {
            //check if magic is enough
            return checkActive() && playerMagic >= Math.Abs(getMagic());

        }

        public bool isHpEnough(int playerHp, double playerDmgVar, int bleedDmg)
        {

            return checkActive() && playerHp > Math.Abs((getHp() + bleedDmg) * (getDamageVariation() + playerDmgVar));
        }

        public bool checkRecepientNeeded()
        {
            return isRecepientNeeded;
        }

        public void setRecepientNeeded(bool needsRecepient)
        {
            this.isRecepientNeeded = needsRecepient;
        }

        public float getCastTime()
        {
            return castSlot;
        }

        public void setCastSlot(float castSlot)
        {
            this.castSlot = castSlot;
        }

        public float getX()
        {
            return x;
        }

        public void setX(float x)
        {
            this.x = x;
        }

        public float getY()
        {
            return y;
        }

        public void setY(float y)
        {
            this.y = y;
        }

        public bool checkCoordinateNeeded()
        {
            return isCoordinateNeeded;
        }

        public void setCoordinateNeeded(bool isCoordinateNeeded)
        {
            this.isCoordinateNeeded = isCoordinateNeeded;
        }

        public bool checkForEnemy()
        {
            return forEnemy;
        }

        public void setForEnemy(bool forEnemy)
        {
        this.forEnemy = forEnemy;
        }

        public abstract void cast(int casterID, int recepientID, float x, float y, float castSlot);
        
        public void setup(float castSlot)
        {
            setCastSlot(castSlot);
        }
        public abstract void run();

        public abstract string spellInfo();

}
