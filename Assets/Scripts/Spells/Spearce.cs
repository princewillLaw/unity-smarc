﻿using UnityEngine;

public class Spearce : Spell {

    

    public Spearce() : base(-60, -60, 0, 0, 0, 0, 0, 0,
                6, 0, false, true, 0.0, 0, 0,
                            "Spearce", true, 0, 0){

    }

    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        base.setup(castSlot);
        base.setInvolvedIDs(casterID, recepientID);
        base.setActive(true);
        run();
    }

    public override void run()
    {
        if (base.isMagicEnough(Server.getPlayers()[getCasterID()].getMagic()))
        {
            //takes magic whether spell is in range or not
            Server.getPlayers()[base.getCasterID()].addMagic(base.getMagic());
            if ((base.getRange()/2)+0.5f >=
                Server.OffsetPos(Server.getPlayers()[base.getCasterID()],
                                 Server.getPlayers()[base.getRecepientID()]))
            {
                Server.getPlayers()[base.getRecepientID()].addHp(base.getHp());
                if (getCasterID() != getRecepientID())
                    Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<RotationScript>().target
                    = Server.getPlayers()[base.getRecepientID()].getGameObject();
                Server.getPlayers()[base.getCasterID()].getGameObject().AddComponent<Spearceing>();
                Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<Spearceing>().target
                    = Server.getPlayers()[base.getRecepientID()].getGameObject();
            }
            else { //out of range
                GameObject.Instantiate(getSpellFailed(), Server.getPlayers()[base.getCasterID()].getGameObject().transform);
            }
        }else { //out of mana
            GameObject.Instantiate(getSpellFailed(), Server.getPlayers()[base.getCasterID()].getGameObject().transform);
        }
        base.setActive(false);
    }

    public override string spellInfo()
    {

        string information = getName() + "\n\n";
        information += "Info: Launch a fierce sword strike\nat a close enemy.\n";
        information += "Magic: " + getMagic() + "\n";
        information += "Damage: " + getHp() + "\n";
        information += "Radius: " + getRange() / 2 + "\n";

        return information;
    }

}

