﻿using UnityEngine;

public class Spearceing : MonoBehaviour
{

    GameObject spearcePrefab,spearceGO,swordGO;
    public GameObject target = null;
    // Start is called before the first frame update
    void Start()
    {
        spearcePrefab = (GameObject)Resources.Load("Spell/spearce", typeof(GameObject));
        swordGO = (GameObject)gameObject.transform.Find("right_hand/elbow/fist/sword").gameObject;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null) {
            spearceGO = Instantiate(spearcePrefab);
            spearceGO.transform.position = swordGO.transform.position;
            spearceGO.GetComponent<spinningSword>().target = target;
            Destroy(gameObject.GetComponent<Spearceing>());
        }
    }
}
