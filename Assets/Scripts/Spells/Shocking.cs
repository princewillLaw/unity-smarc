﻿using UnityEngine;

public class Shocking : MonoBehaviour
{
    GameObject shockPrefab, shockGO;
    // Start is called before the first frame update
    void Start()
    {
        shockPrefab = (GameObject)Resources.Load("Spell/shock", typeof(GameObject));
        shockGO = Instantiate(shockPrefab, transform);
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject.GetComponent<Shocking>(),3f);
        Destroy(shockGO, 3f);
    }
}
