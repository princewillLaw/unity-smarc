﻿using UnityEngine;

public class Grind : Spell
{

    bool hasRan = false;
    bool isDeactivated = true;
    
    public Grind() : base(-25, 0, 0, -50, 0, 0, 40, 0,
                0, 0, false, true, 0.0, 0, 0,
                          "Grind", false, 0, 0){
    }

    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        base.setup(castSlot);

        if (base.checkActive())
        {
            base.setActive(false);
            deactivate();
        }
        else
        {
            base.setInvolvedIDs(casterID, casterID);
            base.setActive(true);
            hasRan = false;
            run();
        }

    }

    public override void run()
    {
        isDeactivated = false;
        //reduce damage attack once
        if (!hasRan)
        {
            Server.getPlayers()[base.getRecepientID()].addAttackDamage(base.getAttackDamage());
            Server.getPlayers()[base.getRecepientID()].addCharge(base.getCharge());
            Server.getPlayers()[base.getCasterID()].getGameObject().AddComponent<Grinding>();
            hasRan = true;
        }
        //decrease caster hp every turn its active
        Server.getPlayers()[base.getCasterID()].addHp(base.getHp());
        if (Server.getPlayers()[base.getCasterID()].getHp() < 0)
        {
            Server.getPlayers()[base.getCasterID()].setHp(1);
            Server.getPlayers()[base.getCasterID()].setHpFinish(false);
        }
        //check if player has movement speed
        if (Server.getPlayers()[base.getCasterID()].getMovement() != 0)
        {
            //add the movement to what we already have
            base.setMovement(base.getMovement() +
                                Server.getPlayers()[base.getCasterID()].getMovement());
            //set the movement to 0
            Server.getPlayers()[base.getCasterID()].setMovement(0);
        }
    }

    public void deactivate()
    {
        if (!isDeactivated)
        {
            Server.getPlayers()[base.getRecepientID()].addAttackDamage(-base.getAttackDamage());
            Server.getPlayers()[base.getRecepientID()].addCharge(-base.getCharge());
            Server.getPlayers()[base.getRecepientID()].addMovement(base.getMovement());
            base.setMovement(0);
            Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<Grinding>().isOn = false;
            hasRan = false;
            isDeactivated = true;
        }
    }
    public override string spellInfo()
    {
        string information = getName() + "\n\n";
        information += "Info: Ramp your magic\nback faster with some\ndraw backs.\n";
        information += "Hp Drain: " + getHp() + " per turn\n";
        information += "Movement is removed\n";
        information += "Attack Reduction: " + getAttackDamage() + "\n";
        information += "Charge Increase: " + getCharge() + "\n";
        return information;
    }

}
