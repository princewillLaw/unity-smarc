﻿using UnityEngine;

public class Berserk : Spell{

    bool hasRan = false;
    bool isDeactivated = true;

    public Berserk() : base(-20, 0, 0, 40, 3, 0, 0, 0,
                0, 0, false, true, 0.5, 0, 0,
              "Berserk", false, 0, 0){
    }


    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        base.setup(castSlot);
        if (base.checkActive())
        {
            base.setActive(false);
            deactivate();
        }
        else
        {
            base.setInvolvedIDs(casterID, casterID);
            base.setActive(true);
            hasRan = false;
            run();
        }
    }

    public override void run()
    {
        isDeactivated = false;
        //increase attack damage and movement only once
        if (!hasRan)
        {
            Server.getPlayers()[base.getCasterID()].addAttackDamage(base.getAttackDamage());
            Server.getPlayers()[base.getCasterID()].addMovement(base.getMovement());
            Server.getPlayers()[base.getCasterID()].addDamageVariation(base.getDamageVariation());
            Server.getPlayers()[base.getCasterID()].getGameObject().AddComponent<Berserking>();
            hasRan = true;
        }
        //decrease caster hp every turn its active
        Server.getPlayers()[base.getCasterID()].addHp(base.getHp());
        //if it killed you set hp to 1
        if (Server.getPlayers()[base.getCasterID()].getHp() < 0 ) {
            Server.getPlayers()[base.getCasterID()].setHp(1);
            Server.getPlayers()[base.getCasterID()].setHpFinish(false);
        }
       
    }

    public void deactivate()
    {
        if (!isDeactivated)
        {
            Server.getPlayers()[base.getCasterID()].addAttackDamage(-base.getAttackDamage());
            Server.getPlayers()[base.getCasterID()].addMovement(-base.getMovement());
            Server.getPlayers()[base.getCasterID()].addDamageVariation(-base.getDamageVariation());
            Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<Berserking>().isOn = false;
            hasRan = false;
            isDeactivated = true;
        }
    }

    public override string spellInfo()
    {

        string information = getName() + "\n\n";
        information += "Info: Abandon all defense,\nEnter a rage Frenzy.\n";
        information += "Hp drain: " + getHp() + " per turn\n";
        information += "Attack Damage Bonus: " + getAttackDamage() + "\n";
        information += "Movement Bonus: " + getMovement() + "\n";
        information += "Damage Variation: " + getDamageVariation() + "\n";

        return information;
    }

}
