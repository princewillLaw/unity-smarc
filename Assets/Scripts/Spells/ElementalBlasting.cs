﻿using UnityEngine;

public class ElementalBlasting : MonoBehaviour
{
    GameObject elementalBlastPrefab, elementalBlastGO, staffGO;
    public GameObject target = null;
    // Start is called before the first frame update
    void Start()
    {
        elementalBlastPrefab = (GameObject)Resources.Load("Spell/elementalBlast", typeof(GameObject));
        staffGO = (GameObject)gameObject.transform.Find("right_hand/elbow/fist/staff").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            elementalBlastGO = Instantiate(elementalBlastPrefab);
            elementalBlastGO.transform.position = staffGO.transform.position;
            elementalBlastGO.GetComponent<spinningSword>().target = target;
            Destroy(gameObject.GetComponent<ElementalBlasting>());
        }
    }
}
