﻿using UnityEngine;

public class ElementalBlast : Spell {

    public ElementalBlast() : base(-50, -90, 0, 0, 0, 0, 0, 0,
                10, 0, false, true, 0.0, 0, 0,
                                   "Elemental Blast", true, 0, 0){}

    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        base.setup(castSlot);
        base.setInvolvedIDs(casterID, recepientID);
        base.setActive(true);
        run();
    }

    public override void run()
    {
        if (base.isMagicEnough(Server.getPlayers()[getCasterID()].getMagic()))
        {
            //takes magic whether spell is in range or not
            Server.getPlayers()[base.getCasterID()].addMagic(base.getMagic());
            if (((base.getRange()/2)+0.5f) >=
                Server.OffsetPos(Server.getPlayers()[base.getCasterID()],
                                 Server.getPlayers()[base.getRecepientID()]))
            {

                Server.getPlayers()[base.getRecepientID()].addHp(base.getHp());
                if (getCasterID() != getRecepientID())
                    Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<RotationScript>().target
                    = Server.getPlayers()[base.getRecepientID()].getGameObject();
                Server.getPlayers()[base.getCasterID()].getGameObject().AddComponent<ElementalBlasting>();
                Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<ElementalBlasting>().target
                    = Server.getPlayers()[base.getRecepientID()].getGameObject();
                randomizeElement();
            }
            else
            { //out of range
                GameObject.Instantiate(getSpellFailed(), Server.getPlayers()[base.getCasterID()].getGameObject().transform);
            }
        }
        else
        { //out of mana
            GameObject.Instantiate(getSpellFailed(), Server.getPlayers()[base.getCasterID()].getGameObject().transform);
        }
        base.setActive(false);
    }

    public void randomizeElement()
    {
        int element = Spell.rnd.Next(3, 7);
        Server.getPlayers()[base.getCasterID()].getSpells()[element].cast(base.getCasterID(), base.getRecepientID(), 0,0, base.getCastTime());
    }

    public override string spellInfo()
    {
        string information = getName() + "\n\n";
        information += "Info: Launch an ever changing\nElemental force at your foe.\n";
        information += "Magic: " + getMagic() + "\n";
        information += "Damage: " + getHp() + "\n";
        information += "Note: Spawn an element to \nweaken your foes, few seconds\nafter contact\n";
        return information;
    }

}
