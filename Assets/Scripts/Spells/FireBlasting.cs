﻿using UnityEngine;

public class FireBlasting : MonoBehaviour
{
    public bool isOn = true;
    GameObject fireBlastPrefab, fireBlastGO;
    // Start is called before the first frame update
    void Start()
    {
        fireBlastPrefab = (GameObject)Resources.Load("Spell/fireBlast", typeof(GameObject));
        fireBlastGO = Instantiate(fireBlastPrefab, transform);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isOn)
        {
            Destroy(fireBlastGO);
            Destroy(gameObject.GetComponent<FireBlasting>());
        }
    }
}
