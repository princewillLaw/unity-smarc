﻿using UnityEngine;
public class WindBlast : Spell {

    bool hasRan = false;
    public WindBlast() : base(0, 0, 0, -30, 0, 0, 0, 0,
                0, 0, false, false, 0.0, 0, 0,
                              "Wind Blast", false, 0, 0){}

    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        base.setup(castSlot);
        base.setDuration(3);
        if (base.checkActive())
        {
            deactivate();
            cast(casterID, recepientID, x, y, castSlot);
        }
        else
        {
            base.setInvolvedIDs(casterID, recepientID);
            base.setActive(true);
            hasRan = false;
        }

        Debug.Log("blast cast");

    }

    public override void run()
    {
        if (base.checkActive() && base.getDuration() > 0 && !hasRan)
        {
            Server.getPlayers()[base.getRecepientID()].addAttackDamage(base.getAttackDamage());
            Server.getPlayers()[base.getRecepientID()].getGameObject().AddComponent<WindBlasting>();
            hasRan = true;
        }

        base.addDuration(-1);

        if (base.getDuration() < 1)
        {
            deactivate();
        }
    }

    public void deactivate()
    {
        Server.getPlayers()[base.getRecepientID()].addAttackDamage(-base.getAttackDamage());
        Server.getPlayers()[base.getRecepientID()].getGameObject().GetComponent<WindBlasting>().isOn = false;
        base.setActive(false);
    }
    public override string spellInfo()
    {
        string information = getName() + "\n\n";
        information += "Info: One of four strong \nelemental forces. Weaken \nthere attacks.\n";
        information += "Attack drain: " + getAttackDamage() + "\n";
        information += "Duration: " + 3 + " turns\n";
        return information;
    }

}
