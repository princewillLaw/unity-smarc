﻿using UnityEngine;

public class Teleporting : MonoBehaviour
{

    GameObject teleportPrefab, teleportGO,chargePrefab;
    public GameObject target = null;
    public bool showTeleport = false,isOn = true,deactivate = false;
    // Start is called before the first frame update
    void Start()
    {
        teleportPrefab = (GameObject)Resources.Load("Spell/teleport", typeof(GameObject));
        chargePrefab = (GameObject)Resources.Load("charge", typeof(GameObject));
    }

 // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            setupTeleporting();
            if (!isOn) {
                //blink player
                GameObject sparks =Instantiate(chargePrefab);
                sparks.transform.position = target.transform.position;
                //maybe rduce the sparks scale
                foreach (Character c in Server.getLivePlayers()) {
                    if (c.getName() + "." + c.getId() == target.name) {
                        target.transform.position = new Vector3(c.getPositionX(), target.transform.position.y, c.getPositionY());
                        break;
                    }
                }
                Destroy(teleportGO);
                Destroy(gameObject.GetComponent<Teleporting>());
            }
            if (deactivate) {
                Destroy(teleportGO);
                Destroy(gameObject.GetComponent<Teleporting>());
            }
        }
    }

    void setupTeleporting() {
        if (!showTeleport) {
            teleportGO = Instantiate(teleportPrefab, target.transform);
            showTeleport = true;
        }

    }
}
