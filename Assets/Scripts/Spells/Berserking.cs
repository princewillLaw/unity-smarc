﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Berserking : MonoBehaviour
{
    public bool isOn = true,isSet = false;
    public Material berserkMat, currentMat;
    // Start is called before the first frame update
    void Start()
    {
        berserkMat =  (Material)Resources.Load("Spell/berserkMat", typeof(Material));
    }

    // Update is called once per frame
    void Update()
    {
        if (isOn)
        {
            setCurrentMaterial();
            
            gameObject.GetComponent<Renderer>().material = berserkMat;
            if (transform.localScale.y <= .8f)
            {
                transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime, transform.localScale.y + Time.deltaTime, transform.localScale.z + Time.deltaTime);
            }
        }
        else {
            gameObject.GetComponent<Renderer>().material = currentMat;
            transform.localScale = new Vector3(.5f, .5f, .5f);
            Destroy(gameObject.GetComponent<Berserking>());
        }
    }

    private void setCurrentMaterial() {
        if (!isSet)
        {
            currentMat = gameObject.GetComponent<Renderer>().material;
            isSet = true;
        }
    }
}
