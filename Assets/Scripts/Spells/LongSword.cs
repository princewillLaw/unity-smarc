﻿using UnityEngine;

public class LongSword : Spell {

    bool hasRan = false;
    bool isDeactivated = true;


    public LongSword() : base(0, -30, 2, 0, 0, 0, 0, 0,
            0, 0, false, true, 0.0, 0, 0,
                              "Long Sword", false, 0, 0){
    }


    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        base.setup(castSlot);

        if (base.checkActive())
        {
            base.setActive(false);
            deactivate();
        }
        else
        {
            base.setInvolvedIDs(casterID, casterID);
            base.setActive(true);
            hasRan = false;
            run();
        }
    }

    public override void run()
    {
        //check if magic is enough and spell is active
        if (base.isMagicEnough(Server.getPlayers()[getRecepientID()].getMagic()))
        {
            isDeactivated = false;
            //increase attack range only once
            if (!hasRan)
            {
                Server.getPlayers()[base.getRecepientID()].addAttackRange(base.getAttackRange());
                Server.getPlayers()[base.getCasterID()].getGameObject().AddComponent<LongSwording>();
            }
            //decrease caster magic every turn its active
            Server.getPlayers()[base.getCasterID()].addMagic(base.getMagic());
            //check if its ran at all this turn
            hasRan = true;
        }
        else
        { //out of mana
            GameObject.Instantiate(getSpellFailed(), Server.getPlayers()[base.getCasterID()].getGameObject().transform);
            deactivate();
        }
    }

    public void deactivate()
    {
        if (!isDeactivated)
        {
            Server.getPlayers()[base.getRecepientID()].addAttackRange(-base.getAttackRange());
            Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<LongSwording>().isOn = false;
            hasRan = false;
            isDeactivated = true;
        }
    }

    public override string spellInfo()
    {

        string information = getName() + "\n\n";
        information += "Info: Focus your Sword Skill to \nincrease the range of attacks.\n";
        information += "Magic: " + getMagic() + " per turn\n";
        information += "Attack Range Bonus: " + getAttackRange() + "\n";

        return information;
    }

}

