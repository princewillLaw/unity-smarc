﻿using UnityEngine;

public class IceBlasting : MonoBehaviour
{
    public bool isOn = true;
    GameObject iceBlastPrefab, iceBlastGO;
    // Start is called before the first frame update
    void Start()
    {
        iceBlastPrefab = (GameObject)Resources.Load("Spell/iceBlast", typeof(GameObject));
        iceBlastGO = Instantiate(iceBlastPrefab, transform);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isOn)
        {
            Destroy(iceBlastGO);
            Destroy(gameObject.GetComponent<IceBlasting>());
        }
    }
}
