﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testScript : MonoBehaviour
{
    System.Random rnd = new System.Random();
    // Start is called before the first frame update
    void Start()
    {
        Server.getPlayers().Add(new Dwarf("dwarfmiester", 0,rnd));

        Debug.Log("Player name:" + Server.getPlayers()[0].getName());
        Debug.Log("Player current magic:"+ Server.getPlayers()[0].getMagic());
        Debug.Log("Player current atk dmg:" + Server.getPlayers()[0].getAttackDamage());
        Debug.Log("Player current y pos:" + Server.getPlayers()[0].getPositionY());

        //Debug.Log("Player cast warcry on self");
        //Server.getPlayers()[0].getSpells()[0].cast(0, 0, 0, 0, 0);

        List<SlotAction> sActions = new List<SlotAction>();
        //sActions.Add(new SlotAction(Server.getPlayers()[0],null,'s',0,0,"War Cry",0,7.656f));
        sActions.Add(new SlotAction(Server.getPlayers()[0], null, 'm', 0, -2, "", 0, 4.656f));
        sActions.Add(new SlotAction(Server.getPlayers()[0], null, 'm', 0, -2, "", 1, 4.656f));
        sActions.Add(new SlotAction(Server.getPlayers()[0], null, 'm', 0, -2, "", 2, 4.656f));
        //sActions.Add(new SlotAction(Server.getPlayers()[0], null, 'r', 0, 0, "", 2, 9.656f));

        Server.AddPlayerSlotActions(sActions);
        Server.setupSlotActions();

        Debug.Log("Player current magic:" + Server.getPlayers()[0].getMagic());
        Debug.Log("Player current atk dmg:" + Server.getPlayers()[0].getAttackDamage());
        Debug.Log("Player current y pos:" + Server.getPlayers()[0].getPositionY());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
