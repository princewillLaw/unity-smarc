﻿using UnityEngine;
public class FireBlast : Spell {

    public FireBlast() : base(-10, 0, 0, 0, 0, 0, 0, 0,
                0, 0, false, false, 0.0, 0, 0,
                              "Fire Blast", false, 0, 0){}

    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        base.setup(castSlot);
        base.setDuration(3);
        base.setInvolvedIDs(casterID, recepientID);
        base.setActive(true);
        
    }

    public override void run()
    {

        if (base.checkActive() && base.getDuration() > 0)
        {
            Server.getPlayers()[base.getRecepientID()].addHp(base.getHp());
            if (Server.getPlayers()[base.getRecepientID()].getGameObject().GetComponent<FireBlasting>() == null)
            {
                Server.getPlayers()[base.getRecepientID()].getGameObject().AddComponent<FireBlasting>();
            }
        }

        base.addDuration(-1);

        if (base.getDuration() < 1)
        {
            Server.getPlayers()[base.getRecepientID()].getGameObject().GetComponent<FireBlasting>().isOn = false;
            base.setActive(false);
        }
    }
    public override string spellInfo()
    {
        string information = getName() + "\n\n";
        information += "Info: One of four strong \nelemental forces. Drain your \nfoes hp.\n";
        information += "Hp drain: " + getHp() + " per turn\n";
        information += "Duration: " + 3 + " turns\n";
        return information;
    }

}
