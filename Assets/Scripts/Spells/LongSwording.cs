﻿using UnityEngine;

public class LongSwording : MonoBehaviour
{
    public bool isOn = true, isSet = false;
    public Material LongSwordMat, currentMat;
    GameObject swordGO,swordGlow,swordGlowGO;
    Transform swordT;
    // Start is called before the first frame update
    void Start()
    {
        LongSwordMat = (Material)Resources.Load("Spell/longSwordMat", typeof(Material));
        swordGlow = (GameObject)Resources.Load("swordGlow", typeof(GameObject));
        swordGO = (GameObject)gameObject.transform.Find("right_hand/elbow/fist/sword").gameObject;
        swordT = swordGO.transform;
        swordGlowGO = (GameObject)Instantiate(swordGlow, swordT);
    }


    // Update is called once per frame
    void Update()
    {
        if (isOn)
        {
            setCurrentMaterial();

            swordGO.GetComponent<Renderer>().material = LongSwordMat;
            if (swordT.localScale.y <= 8f)
            {
                swordT.localScale = new Vector3(swordT.localScale.x, swordT.localScale.y + Time.deltaTime, swordT.localScale.z);
                
            }
        }
        else
        {
            swordGO.GetComponent<Renderer>().material = currentMat;
            swordT.localScale = new Vector3(.5f, 5f, .03f);
            Destroy(swordGlowGO);
            Destroy(gameObject.GetComponent<LongSwording>());
        }
    }

    private void setCurrentMaterial()
    {
        if (!isSet)
        {
            currentMat = swordGO.GetComponent<Renderer>().material;
            isSet = true;
        }
    }
}
