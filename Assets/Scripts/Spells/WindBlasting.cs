﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindBlasting : MonoBehaviour
{
    public bool isOn = true;
    GameObject windBlastPrefab, windBlastGO;
    // Start is called before the first frame update
    void Start()
    {
        windBlastPrefab = (GameObject)Resources.Load("Spell/windBlast", typeof(GameObject));
        windBlastGO = Instantiate(windBlastPrefab, transform);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isOn)
        {
            Destroy(windBlastGO);
            Destroy(gameObject.GetComponent<WindBlasting>());
        }
    }
}
