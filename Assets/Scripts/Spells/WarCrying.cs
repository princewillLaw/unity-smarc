﻿using UnityEngine;

public class WarCrying : MonoBehaviour
{
    GameObject warcryPrefab, warcryGO;
    public GameObject target = null;
    public bool showUpgrade = false, isOn = true;

    // Start is called before the first frame update
    void Start()
    {
        warcryPrefab = (GameObject)Resources.Load("Spell/warcry", typeof(GameObject));
        warcryGO = Instantiate(warcryPrefab, transform);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isOn)
        {
            Destroy(warcryGO);
            Destroy(gameObject.GetComponent<WarCrying>());
        }
    }
}
