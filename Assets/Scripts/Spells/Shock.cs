﻿using UnityEngine;

public class Shock : Spell {

    public Shock() : base(-75,-100,0,0,0,0,0,0,
                0,3,false,true,0.0,0,0,
                          "Shock",false,0,0){
    }

    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        base.setup(castSlot);
        base.setInvolvedIDs(casterID, casterID);
        base.setActive(true);
        run();
    }
    
    public override void run() {
        if(base.isMagicEnough(Server.getPlayers()[getCasterID()].getMagic())) {
            //takes magic whether spell is in range or not
            Server.getPlayers()[base.getCasterID()].addMagic(base.getMagic());

            foreach (Character c in  Server.getPlayers()) {
                if(Server.OffsetPos(Server.getPlayers()[base.getCasterID()],c)<getRadius() //distance between players is less than radius
                    && c != Server.getPlayers()[base.getCasterID()]) {//and players are not the same person
                    c.addHp(base.getHp());
                }
            }

            Server.getPlayers()[base.getCasterID()].getGameObject().AddComponent<Shocking>();
        }
        else{ //out of mana
            GameObject.Instantiate(getSpellFailed(), Server.getPlayers()[base.getCasterID()].getGameObject().transform);
        }
        base.setActive(false);
    }

    public override string spellInfo()
    {
        string information = getName() + "\n\n";
        information += "Info: Slam the ground\nso hard it creates\na damaging shockwave.\n";
        information += "Damage: " + getHp() + " per turn\n";
        information += "Radius: " + getRadius() + "\n";
        information += "Magic: " + getMagic() + "\n";
        return information;
    }

}
