﻿using UnityEngine;

public class WarCry : Spell {

    bool hasRan = false;
    bool isDeactivated = true;

    public WarCry() : base(0, -35, 0, 20, 2, 0, 0, 0,
                0, 0, false, true, 0.0, 0, 0,
                           "War Cry", false, 0, 0){
    }

    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        base.setup(castSlot);
        if (base.checkActive())
        {
            base.setActive(false);
            deactivate();
        }
        else
        {
            base.setInvolvedIDs(casterID, casterID);
            base.setActive(true);
            hasRan = false;
            run();
        }

    }

    public override void run()
    {
        
        //check if magic is enough and spell is active
        if (base.isMagicEnough(Server.getPlayers()[getRecepientID()].getMagic()))
        {
            isDeactivated = false;
            //reduce damage variation and movement only once
            if (!hasRan)
            {
                Server.getPlayers()[base.getRecepientID()].addAttackDamage(base.getAttackDamage());
                Server.getPlayers()[base.getRecepientID()].addMovement(base.getMovement());
                Server.getPlayers()[base.getRecepientID()].getGameObject().AddComponent<WarCrying>();
            }
            //decrease caster magic every turn its active
            Server.getPlayers()[base.getCasterID()].addMagic(base.getMagic());

            //check if its ran at all this turn
            hasRan = true;
        }
        else
        { //out of mana
            GameObject.Instantiate(getSpellFailed(), Server.getPlayers()[base.getCasterID()].getGameObject().transform);
            deactivate();
        }
    }

    public void deactivate()
    {
        if (!isDeactivated)
        {
            Server.getPlayers()[base.getRecepientID()].getGameObject().GetComponent<WarCrying>().isOn = false;
            Server.getPlayers()[base.getRecepientID()].addAttackDamage(-base.getAttackDamage());
            Server.getPlayers()[base.getRecepientID()].addMovement(-base.getMovement());
            hasRan = false;
            isDeactivated = true;
        }
    }

    public override string spellInfo()
    {
        string information = getName() + "\n\n";
        information += "Info: Roar powerful\ncries to odin\nto boost your strength.\n";
        information += "Magic drain: " + getMagic() + "per turn \n";
        information += "Attack Damage Bonus: " + getAttackDamage() + "\n";
        information += "Movement Bonus: " + getMovement() + "\n";
        return information;
    }

}
