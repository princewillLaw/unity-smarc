﻿using UnityEngine;

public class Upgradeing : MonoBehaviour
{

    GameObject upgradePrefab, upgradeGO;
    public GameObject target = null;
    public bool showUpgrade = false, isOn = true;
    // Start is called before the first frame update
    void Start()
    {
        upgradePrefab = (GameObject)Resources.Load("Spell/upgrade", typeof(GameObject));
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null) {
            setupUpgradeing();
            if (!isOn) {
                //add animation
                Destroy(upgradeGO);
                Destroy(gameObject.GetComponent<Upgradeing>());

            }
        }
    }

    void setupUpgradeing()
    {
        if (!showUpgrade)
        {
            upgradeGO = Instantiate(upgradePrefab, target.transform);
            showUpgrade = true;
        }

    }
}
