﻿using UnityEngine;

public class Guardian : Spell{

    bool hasRan = false;
    bool isDeactivated = true;

    public Guardian() : base(0, -35, 0, 0, -1, 0, 0, 0,
                0, 0, false, true, -0.25, 0, 0,
               "Guardian", false, 0, 0){
    }


    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        base.setup(castSlot);
        
        if (base.checkActive())
        {
            base.setActive(false);
            deactivate();
        }
        else
        {
            base.setInvolvedIDs(casterID, casterID);
            base.setActive(true);
            hasRan = false;
            run();
        }

    }

    public override void run()
    {
        //check if magic is enough and spell is active
        if (base.isMagicEnough(Server.getPlayers()[getRecepientID()].getMagic()))
        {
            isDeactivated = false;
            //reduce damage variation and movement only once
            if (!hasRan)
            {
                Server.getPlayers()[base.getRecepientID()].addDamageVariation(base.getDamageVariation());
                Server.getPlayers()[base.getRecepientID()].addMovement(base.getMovement());
                Server.getPlayers()[base.getCasterID()].getGameObject().AddComponent<Guardianing>();
            }
            //decrease caster magic every turn its active
            Server.getPlayers()[base.getCasterID()].addMagic(base.getMagic());

            //check if its ran at all this turn
            hasRan = true;
        }
        else{ //out of mana
            GameObject.Instantiate(getSpellFailed(), Server.getPlayers()[base.getCasterID()].getGameObject().transform);
            deactivate();
        }
    }

    public void deactivate()
    {
        if (!isDeactivated)
        {
            Server.getPlayers()[base.getRecepientID()].addDamageVariation(-base.getDamageVariation());
            Server.getPlayers()[base.getRecepientID()].addMovement(-base.getMovement());
            Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<Guardianing>().isOn = false;
            hasRan = false;
            isDeactivated = true;
        }
    }

    public override string spellInfo() {

        string information = getName()+"\n\n";
        information += "Info: protective stance that \nslows you down but reduces \nall damage.\n";
        information += "Magic: " + getMagic() + " per turn\n";
        information += "Movement Reduction: " + getMovement() + "\n";
        information += "Damage Variation: " + getDamageVariation() + "\n";

        return information;
    }

}
