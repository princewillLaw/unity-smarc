﻿using UnityEngine;

public class Heal : Spell {

    public Heal() : base(40, -75, 0, 0, 0, 0, 0, 0,
                10, 0, false, true, 0.0, 0, 0,
                         "Heal", true, 0, 0){

    }

    public override void cast(int casterID, int recepientID, float x, float y, float castSlot)
    {
        base.setup(castSlot);
        base.setInvolvedIDs(casterID, recepientID);
        base.setActive(true);
        run();
    }

    public override void run()
    {
        if (base.isMagicEnough(Server.getPlayers()[getCasterID()].getMagic()))
        {
            Server.getPlayers()[base.getCasterID()].addMagic(base.getMagic());
            if ((base.getRange()/2)+0.5f >=
                Server.OffsetPos(Server.getPlayers()[base.getCasterID()],
                                 Server.getPlayers()[base.getRecepientID()]))
            {

                Server.getPlayers()[base.getRecepientID()].addHp(base.getHp());
                if (getCasterID() != getRecepientID())
                    Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<RotationScript>().target
                    = Server.getPlayers()[base.getRecepientID()].getGameObject();
                Server.getPlayers()[base.getCasterID()].getGameObject().AddComponent<Healing>();
                Server.getPlayers()[base.getCasterID()].getGameObject().GetComponent<Healing>().target
                    = Server.getPlayers()[base.getRecepientID()].getGameObject();

            }
            else
            { //out of range
                GameObject.Instantiate(getSpellFailed(), Server.getPlayers()[base.getCasterID()].getGameObject().transform);
            }
        }
        else
        { //out of mana
            GameObject.Instantiate(getSpellFailed(), Server.getPlayers()[base.getCasterID()].getGameObject().transform);
        }
        base.setActive(false);
    }
    public override string spellInfo()
    {
        string information = getName() + "\n\n";
        information += "Info: Persist your allies with \nthis powerful support spell.\n";
        information += "Magic: " + getMagic() + "\n";
        information += "Hp Heal: " + getHp() + "\n";
        information += "Radius: " + getRange() / 2 + "\n";
        return information;
    }

}
