﻿using UnityEngine;

public class Healing : MonoBehaviour
{
    GameObject healPrefab, healGO;
    public GameObject target = null;
    bool showHeal = false;
    // Start is called before the first frame update
    void Start()
    {
        healPrefab = (GameObject)Resources.Load("Spell/heal", typeof(GameObject));

    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            setupHealing();
            Destroy(healGO, 2.5f);
            Destroy(gameObject.GetComponent<Healing>(),2.5f);
        }
    }

    void setupHealing() {
        if (!showHeal) {
            healGO = Instantiate(healPrefab,target.transform);
            showHeal = true;
        }

    }
}
