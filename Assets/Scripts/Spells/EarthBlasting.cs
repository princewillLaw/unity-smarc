﻿using UnityEngine;

public class EarthBlasting : MonoBehaviour
{
    public bool isOn = true;
    GameObject earthBlastPrefab, earthBlastGO;
    // Start is called before the first frame update
    void Start()
    {
        earthBlastPrefab = (GameObject)Resources.Load("Spell/earthBlast", typeof(GameObject));
        earthBlastGO = Instantiate(earthBlastPrefab, transform);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isOn) {
            Destroy(earthBlastGO);
            Destroy(gameObject.GetComponent<EarthBlasting>());
        }
    }
}
