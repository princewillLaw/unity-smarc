﻿using UnityEngine;

public class Grinding : MonoBehaviour
{

    GameObject grindPrefab, grindGO;
    public bool isOn = true,showGrind = false;
    // Start is called before the first frame update
    void Start()
    {
        grindPrefab = (GameObject)Resources.Load("Spell/grind", typeof(GameObject));
    }

    // Update is called once per frame
    void Update()
    {
        setupGrinding();
        if (!isOn) {
            grindGO.transform.localScale = new Vector3(grindGO.transform.localScale.x - Time.deltaTime, grindGO.transform.localScale.y - Time.deltaTime, grindGO.transform.localScale.z - Time.deltaTime);
            if (grindGO.transform.localScale.x < 0f) {
                Destroy(grindGO);
                Destroy(gameObject.GetComponent<Grinding>());
            }
        }
    }

    void setupGrinding()
    {
        if (!showGrind)
        {
            grindGO = Instantiate(grindPrefab, transform);
            showGrind = true;
        }

    }
}
