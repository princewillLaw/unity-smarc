﻿using UnityEngine;

public class Guardianing : MonoBehaviour
{
    public bool isOn = true, isSet = false;
    public Material GuardianMat, currentMat;
    GameObject shieldGO;
    Transform shieldT;
    // Start is called before the first frame update
    void Start()
    {
        GuardianMat = (Material)Resources.Load("Spell/guardianMat", typeof(Material));
        shieldGO = (GameObject)gameObject.transform.Find("left_hand/elbow/fore/shield").gameObject;
        shieldT = shieldGO.transform;
    }


    // Update is called once per frame
    void Update()
    {
        if (isOn)
        {
            setCurrentMaterial();

            shieldGO.GetComponent<Renderer>().material = GuardianMat;
            if (shieldT.localScale.y <= 11f)
            {
                shieldT.localScale = new Vector3(shieldT.localScale.x + Time.deltaTime, shieldT.localScale.y + Time.deltaTime, shieldT.localScale.z);
            }
        }
        else
        {
            shieldGO.GetComponent<Renderer>().material = currentMat;
            shieldT.localScale = new Vector3(4f, 6f, .2f);
            Destroy(gameObject.GetComponent<Guardianing>());
        }
    }

    private void setCurrentMaterial()
    {
        if (!isSet)
        {

            currentMat = shieldGO.GetComponent<Renderer>().material;
            isSet = true;
        }
    }
}
