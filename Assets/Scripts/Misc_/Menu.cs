﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene("CharacterScene", LoadSceneMode.Single);
    }

    public void CloseGame()
    {
        Application.Quit();
    }
}
