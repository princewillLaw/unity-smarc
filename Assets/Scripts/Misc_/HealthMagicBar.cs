﻿using UnityEngine;
using UnityEngine.UI;

public class HealthMagicBar : MonoBehaviour
{

    public Text healthText, magicText;
    public RectTransform healthBar, magicBar;
    public GameObject barPanel;

    private GameObject range;

    // Update is called once per frame
    void Update()
    {
        barPanel.SetActive(false);
        if (range != null)
            Destroy(range);
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            
            string Name = hit.collider.name;
            if (Name == "Ground") 
                return;
                

            foreach (Character c in Server.getLivePlayers())
            {
                if (c.getName() + "." + c.getId() == Name)
                {
                    range = Instantiate((GameObject)Resources.Load("Range", typeof(GameObject)));
                    Vector3 playerPos = c.getGameObject().transform.position;
                    range.transform.position = new Vector3(playerPos.x, 0.1f, playerPos.z);
                    int radius = c.getAttackRange();
                    radius = radius < 0 ? 0 : radius;
                    range.transform.localScale = new Vector3(radius, radius, 1f);
                    range.GetComponent<SpriteRenderer>().color = new Color(1f,.8f,0f,0.45f);
                    healthText.text = "" + c.getHp();
                    magicText.text = "" + c.getMagic();
                    healthBar.sizeDelta = new Vector2(c.getHp() / 2, healthBar.sizeDelta.y);
                    magicBar.sizeDelta = new Vector2(c.getMagic() / 2, magicBar.sizeDelta.y);
                    barPanel.SetActive(true);
                    break;
                }

            }
        }
    }


}
