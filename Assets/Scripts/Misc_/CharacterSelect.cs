﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class CharacterSelect : MonoBehaviour
{

    public GameObject slider;
    public GameObject text;
    public GameObject wText, mText, dText;
    public GameObject inputField;
    System.Random rnd = new System.Random();

    Character player;

    private void Start()
    {
        player = new Warrior("Anonymous", 0, rnd);
        player.setPlayer(true);
    }

    private void Update()
    {
        text.GetComponent<Text>().text = "" + (int)slider.GetComponent<Slider>().value;

        if (inputField.GetComponent<Text>().text != null && inputField.GetComponent<Text>().text != "")
                player.setName(inputField.GetComponent<Text>().text);

    }

    private Character CreatePlayer(int characterID, int team) {
        string name = "Anonymous";
        Character c = new Warrior(name, team, rnd);

        if (team != 0){
            
            int aiName = rnd.Next(100,1000);

            switch (characterID)
            {
                case 0:
                    c = new Warrior("Warrior" + aiName, team, rnd);
                    return c;
                case 1:
                    c = new Mage("Mage" + aiName, team, rnd);
                    return c;
                case 2:
                    c = new Dwarf("Dwarf" + aiName, team, rnd);
                    return c;
            }
        }

        switch (characterID) {
            case 0:
                c = new Warrior("Anonymous", team, rnd);
                return c;
            case 1:
                c = new Mage("Anonymous", team, rnd);
                return c;
            case 2:
                c = new Dwarf("Anonymous", team, rnd);
                return c;
            default:
                return c;
        }

    }

    public void SetPlayer(int characterID) {

        player = CreatePlayer(characterID,0);
        player.setPlayer(true);

        whitenTexts();
        switch (characterID) {
            case 0:
                wText.GetComponent<Text>().color = Color.red;
                break;
            case 1:
                mText.GetComponent<Text>().color = Color.red;
                break;
            case 2:
                dText.GetComponent<Text>().color = Color.red;
                break;
            default:
                wText.GetComponent<Text>().color = Color.red;
                break;
        }

    }

    private void whitenTexts() {
        wText.GetComponent<Text>().color = Color.white;
        mText.GetComponent<Text>().color = Color.white;
        dText.GetComponent<Text>().color = Color.white;
    }

    public void SetName(string name) {

        player.setName(name);
            
    }

    public void complete() {
        List<Character> newPlayers = new List<Character>();
        newPlayers.Add(player);
        
        for (int i = 1; i <= (int)slider.GetComponent<Slider>().value; i++) {
            int randomID = rnd.Next(3);
            Character c = CreatePlayer(randomID, i);
            c.setId(i);
            newPlayers.Add(c);
        }

        Server.setPlayers(newPlayers);
        Server.fixOverlappedPlayers();
        //        foreach (Character c in Server.getPlayers()) {
        //           print(c.tostring());
        //        }
        SceneManager.LoadScene("GameplayScene", LoadSceneMode.Single);
    }

}
