﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{

    public float turnSpeed = 4.0f, direction = 0f;
    public Transform player = null;

    Vector3 offset;
    bool hasOffset = false;

    void Start()
    {
       
    }

    void LateUpdate()
    {
        if (player != null && !hasOffset) {
            offset = new Vector3(0, 13.0f,  8.0f);
            hasOffset = true;
        }

        if (Input.GetKey("left")) {
            offset = Quaternion.AngleAxis(-1 * turnSpeed, Vector3.up) * offset;
        }
        if (Input.GetKey("right"))
        {
            offset = Quaternion.AngleAxis(1 * turnSpeed, Vector3.up) * offset;
        }

        transform.position = player.position + offset;
        transform.LookAt(player.position);
    }
}