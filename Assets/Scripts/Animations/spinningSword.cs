﻿using UnityEngine;

public class spinningSword : MonoBehaviour
{
    public float speed = 2500f,movespeed=10f;
    public char axis = 'z';
    public GameObject target;
    void Update()
    {
        switch (axis) {
            case 'x':
                transform.Rotate(speed * Time.deltaTime, 0f, 0f);
                break;
            case 'y':
                transform.Rotate(0f, speed * Time.deltaTime, 0f);
                break;
            case 'z':
                transform.Rotate(0f, 0f, speed * Time.deltaTime);
                break;
            default:
                transform.Rotate(0f, 0f, speed * Time.deltaTime);
                break;
        }

        if (target != null) {
            float step = movespeed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
            if (target.transform.position == transform.position)
            {
                Destroy(gameObject);
            }
        }

    }


}
