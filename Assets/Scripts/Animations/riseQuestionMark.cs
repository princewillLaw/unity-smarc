﻿using UnityEngine;

public class riseQuestionMark : MonoBehaviour
{
    float speed = .1f;

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Camera.main.transform);
        transform.Translate(0f,  (speed * Time.deltaTime), 0f);
        Destroy(gameObject,2f);
    }
}
