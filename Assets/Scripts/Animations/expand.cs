﻿using UnityEngine;

public class expand : MonoBehaviour
{

    public float maxSize = 3f,speed = .1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(transform.localScale.x+(speed*Time.deltaTime), transform.localScale.y+(speed*Time.deltaTime), transform.localScale.z);
        if (transform.localScale.x > maxSize)
            Destroy(gameObject);
    }
}
