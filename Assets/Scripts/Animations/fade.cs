﻿using UnityEngine;

public class fade : MonoBehaviour
{
    float fader = .7f,speed = 0.5f;
    bool show = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (fader < 0.1f) {
            show = true;
        }
        if (fader > .4f)
        {
            show = false;
        }

        fader += (show ? (speed * Time.deltaTime) : -(speed * Time.deltaTime));
        print(fader);
        gameObject.GetComponent<Renderer>().material.SetColor("_Color",new Color(1f,0f,0f,fader));
    }
}
