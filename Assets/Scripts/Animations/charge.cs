﻿using UnityEngine;

public class charge : MonoBehaviour
{
    public GameObject chargeBall;
    float timer = .2f;
    public bool isInfinite = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0f) {
            Instantiate(chargeBall,transform);
            timer = .2f;
        }

        if(!isInfinite)
            Destroy(gameObject, 2f);
    }
}
