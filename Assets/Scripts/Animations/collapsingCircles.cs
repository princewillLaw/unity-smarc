﻿using UnityEngine;

public class collapsingCircles : MonoBehaviour
{
    public float collapsingSpeed,fixedCollapsingSpeed = 10f;
    float collapseAxisValue = 0f;
    private void Start()
    {
        collapseAxisValue = transform.localScale.y;
        collapsingSpeed = fixedCollapsingSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y+(collapsingSpeed*Time.deltaTime), transform.localScale.z);

        if (transform.localScale.y < -collapseAxisValue) {
            collapsingSpeed = fixedCollapsingSpeed;
        }

        if (transform.localScale.y > collapseAxisValue)
        {
            collapsingSpeed = -fixedCollapsingSpeed;
        }
    }
}
