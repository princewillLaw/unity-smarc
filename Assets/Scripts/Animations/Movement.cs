﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    public bool isMoving = false;
    public bool triggered = false;
    public float speed = 4f;
    public Character player = null;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (isMoving && player != null)
        {
            Vector3 target = new Vector3(player.getPositionX(), transform.position.y, player.getPositionY());
            if(player.getPositionX() == transform.position.x && player.getPositionY() == transform.position.z) {
                isMoving = false;
                player = null;
                return;
            }
            float step = speed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, target, step);
            if (transform.position.x == player.getPositionX() && transform.position.z == player.getPositionY()){
                isMoving = false;
                player = null;
            }
        }
        
    }
}
