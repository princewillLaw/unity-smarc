﻿using UnityEngine;

public class RotationScript : MonoBehaviour
{
    public GameObject target;
    //private Vector3 targetPoint;
    //private Quaternion targetRotation;
    //bool isRotating = false;


    // Update is called once per frame
    void Update()
    {
        
        //action.getCaster().getGameObject().transform.Rotate(0, -90, 0);
        if (target != null) {
            rotate();
        }
    }

    void rotate()
    {
        transform.LookAt(target.transform);
        transform.Rotate(0, -90, 0);
        if(target.name == "12131314141515151616161717178181")
            Destroy(target);
        target = null;
    }


}
