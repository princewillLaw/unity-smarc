﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rest : MonoBehaviour
{
    float speed = 1.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y <1.5f) {
            transform.Translate(transform.up*Time.deltaTime*speed);
        }
        else {
            Destroy(gameObject); 
        }
    }
}
