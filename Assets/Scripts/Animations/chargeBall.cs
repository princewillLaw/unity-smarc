﻿using UnityEngine;

public class chargeBall : MonoBehaviour
{
    float x, y, z;
    float speed = .1f;
    // Start is called before the first frame update
    void Start()
    {
        x = Random.Range(-10.0f, 10.0f);
        y = Random.Range(-10.0f, 10.0f);
        z = Random.Range(-10.0f, 10.0f);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = new Vector3(x, y, z);
        transform.Translate(direction * speed * Time.deltaTime);
        Destroy(gameObject, .25f);
    }
}
