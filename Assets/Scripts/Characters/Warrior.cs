﻿using System;
using System.Collections.Generic;

public class Warrior : Character
{

    public Warrior(string name, int team, System.Random rnd) : base(name, 0, team, 400, 250, 6, 3, 65,
                  0, 0, 25, 45, "Warrior", 1.0, false, new List<Spell>(){
                     new Guardian(),
                     new LongSword(),
                     new Spearce(),
                     new Berserk()}, rnd)
    {}

}
