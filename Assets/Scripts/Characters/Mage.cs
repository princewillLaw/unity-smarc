﻿using System;
using System.Collections.Generic;

public class Mage : Character
{

    public Mage(string name, int team, System.Random rnd) : base(name, 0, team, 300, 300, 4, 5, 35,
               0, 0, 20, 50, "Mage", 1.0, false, new List<Spell>(){
                new ElementalBlast(),
                new Heal(),
                new Teleport(),
                new FireBlast(),
                new EarthBlast(),
                new WindBlast(),
                new IceBlast()}, rnd)
    { }
    
}
