﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Character
{
    
        private string Name;
        private int Id;
        private int Hp;
        private int Magic;
        private float Movement;
        private int AttackRange;
        private int AttackDamage;
        private float PositionX;
        private float PositionY;

        private int Rest;
        private int Charge;

        private string Type;

        private double DamageVariation;
        private bool isHpFinish;
        private bool isPlayer = false;

        private int teamNumber = -1;
        private int bleedDamage = 0;

        private List<Spell> spells = new List<Spell>();

        private GameObject gameObject;

        public Character(){
            
        }

        public Character(string name, int id, int teamNumber, int hp, int magic, int movement, int attackRange, int attackDamage,
                int positionX, int positionY, int rest, int charge, string type, double damageVariation, bool isHpFinish,
                List<Spell> spells, System.Random rnd)
        {

            Name = name;
            Id = id;
            Hp = hp;
            Magic = magic;
            Movement = movement*2;
            AttackRange = attackRange*2;
            AttackDamage = attackDamage;
            Rest = rest;
            Charge = charge;
            Type = type;
            DamageVariation = damageVariation;
            this.isHpFinish = isHpFinish;
            this.spells = spells;
            this.teamNumber = teamNumber;

            PositionX = rnd.Next(0,24);
            PositionY = rnd.Next(0,24);
        }

    public GameObject getGameObject()
    {
        return this.gameObject;
    }

    public void setGameObject(GameObject gameObject)
    {
        this.gameObject = gameObject;
    }

    public string getName()
        {
            return Name;
        }

        public void setName(string name)
        {
            Name = name;
        }

        public int getId()
        {
            return Id;
        }

        public void setId(int iD)
        {
            Id = iD;
        }

        public int getHp()
        {
            return Hp;
        }

        public void addHp(int hpChange)
        {
            if (hpChange < 0)
            {
            Hp += Convert.ToInt32((hpChange + getBleedDamage()) * getDamageVariation());
                addBleedDamage();
            }
            else
                Hp += hpChange;

            setHpCap();
        }

        public void setHp(int newHp)
        {
            Hp = newHp;
            setHpCap();
        }

        private void setHpCap()
        {
            if (Hp <= 0)
                setHpFinish(true);
            if (Hp > 500)
                Hp = 500;
        }

        public bool isDead()
        {
            return isHpFinish;
        }

        public void setHpFinish(bool isHpFinish)
        {
            this.isHpFinish = isHpFinish;
        }

        public int getMagic()
        {
            return Magic;
        }

        public void addMagic(int magicChange)
        {
            Magic += magicChange;
            setMagicCap();
        }



        private void setMagicCap()
        {
            if (Magic < 0)
                Magic = 0;
            if (Magic > 500)
                Magic = 500;
        }

        public float getMovement()
        {
            return Movement;
        }

        public void addMovement(float mOVEMENT)
        {
            Movement += mOVEMENT;
        }
        public void setMovement(float movement)
        {
            Movement = movement;
        }

        public int getAttackRange()
        {
            return AttackRange;
        }

        public void addAttackRange(int aTTACK_RANGE)
        {
            AttackRange += aTTACK_RANGE;
        }

        public int getAttackDamage()
        {
            return AttackDamage;
        }

        public void addAttackDamage(int aTTACK_DAMAGE)
        {
            AttackDamage += aTTACK_DAMAGE;
        }

        public float getPositionX()
        {
            return PositionX;
        }

        public void addPositionX(float posOffset)
        {
            if (PositionX + posOffset > 23)
                PositionX = 23;
            else if (PositionX + posOffset < 0)
                PositionX = 0;
            else
                PositionX += posOffset;
        }

        public void setPositionX(float pOSITION_X)
        {
            if (pOSITION_X > 23)
                pOSITION_X = 23;
            else if (pOSITION_X < 0)
                pOSITION_X = 0;

            PositionX = pOSITION_X;
        }

        public float getPositionY()
        {
            return PositionY;
        }

        public void addPositionY(float posOffset)
        {
            if (PositionY + posOffset > 23)
                PositionY = 23;
            else if (PositionY + posOffset < 0)
                PositionY = 0;
            else
                PositionY += posOffset;
        }

        public void setPositionY(float pOSITION_Y)
        {
            if (pOSITION_Y > 23)
                pOSITION_Y = 23;
            else if (pOSITION_Y < 0)
                pOSITION_Y = 0;

            PositionY = pOSITION_Y;
        }

    public void moveTo(float Dx, float Dy) {


        Vector3 movePoint = new Vector3(getPositionX() + Dx, getGameObject().transform.position.y, getPositionY() + Dy);
        if (Server.OffsetPos(getGameObject().transform.position, movePoint) <= (getMovement() / 2))//+(.5f*(Player.getMovement() == 0 ? 0:1)))
        {
            addPositionX(Dx);
            addPositionY(Dy);
        }
        float x = getPositionX();
        float y = getGameObject().transform.position.y;
        float z = getPositionY();
        GameObject g;
        g = new GameObject();
        g.name = "12131314141515151616161717178181";
        g.transform.position = new Vector3(x, y, z);
        getGameObject().GetComponent<RotationScript>().target = g;

        gameObject.GetComponent<Movement>().isMoving = true;
        gameObject.GetComponent<Movement>().player = this;
    }

        public string getType()
        {
            return Type;
        }

        public void setType(string tYPE)
        {
            Type = tYPE;
        }

        public int getRest()
        {
            return Rest;
        }

        public void addRest(int rEST)
        {
            Rest += rEST;
        }

        public int getCharge()
        {
            return Charge;
        }

        public void addCharge(int cHARGE)
        {
            Charge += cHARGE;
        }


        public double getDamageVariation()
        {
            return DamageVariation;
        }

        public void addDamageVariation(double dAMAGE_VARIATION)
        {
            DamageVariation += dAMAGE_VARIATION;
        }

        public List<Spell> getSpells()
        {
            return spells;
        }

        public void setSpells(List<Spell> spells)
        {
            this.spells = spells;
        }

        public int getTeamNumber()
        {
            return teamNumber;
        }

        public void setTeamNumber(int teamNumber)
        {
            this.teamNumber = teamNumber;
        }

        public int getBleedDamage()
        {
            return bleedDamage;
        }

        public void addBleedDamage()
        {
            if (this.bleedDamage == 0)
            {
                setBleedDamage(-1);
            }
            else
            {
                this.bleedDamage *= 2;
            }
        }

        public void setBleedDamage(int bleedDamage)
        {
            this.bleedDamage = bleedDamage;
        }

        public void rest()
        {
            addHp(getRest());
            setBleedDamage(0);
        try
        {
            
            getGameObject().GetComponent<Animator>().SetTrigger("rest");
        }
        catch (Exception e) { Debug.Log(e.Message); }
    }

        public void charge()
        {
            addMagic(getCharge());
        try
        {
            getGameObject().GetComponent<Animator>().SetTrigger("charge");
        }
        catch (Exception e) { Debug.Log(e.Message); }
    }

        public bool isAPlayer()
        {
            return isPlayer;
        }

        public void setPlayer(bool isPlayer)
        {
            this.isPlayer = isPlayer;
        }
    public string shortStatus() {

        return String.Format("Name: {0} the {1}\nHP: {2} MAGIC: {3}\n"
            , getName(), getType(), getHp(), getMagic());
    }

        public string status()
        {
            return String.Format("Name: {0} the {1}\nHP: {2} MAGIC: {3}\nMVT: {4} X: {5} Y: {6}\nATK; DMG: {7} RNG: {8}\nDMG_VRN: {9} BLD_DMG: {10}\nRST: {11} CHRG: {12}\n"
                , getName(), getType(), getHp(), getMagic(), getMovement(), getPositionX(), getPositionY(), getAttackDamage(), getAttackRange(), getDamageVariation(), getBleedDamage(), getRest(), getCharge());
        }

        public string tostring()
        {
            return String.Format("{0}. {1} {2}. Hp: {3} Magic: {4} X: {5} Y: {6} DMG: {7} RNG: {8} VRN: {9} MVT: {10} BLDDMG: {11}", getId(), (getName() + " the " + getType()), "Team " + getTeamNumber(), getHp(), getMagic(), getPositionX(), getPositionY(), getAttackDamage(), getAttackRange(), getDamageVariation(), getMovement(), getBleedDamage());
        }


}

